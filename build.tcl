# run this like: source /mnt/c/Users/test/source/tis100-core/build.tcl

set partNum xc7z020clg400-1

catch close_project

create_project -in_memory -part $partNum

source [file normalize [info script]/../tcl/read_files.tcl]

# synth_design -top block_top_wrapper -part $partNum
# write_checkpoint -force $outputDir/post_synth.dcp

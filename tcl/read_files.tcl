# read headers
read_verilog -sv [glob [file normalize [info script]/../../fpga/tis100-core.srcs/sources_1/new]/*.svh]
# read my files
read_verilog -sv [glob [file normalize [info script]/../../fpga/tis100-core.srcs/sources_1/new]/*.sv]
read_verilog     [glob [file normalize [info script]/../../fpga/tis100-core.srcs/sources_1/new]/*.v ]
# read IP
puts [file normalize [info script]/../../fpga/tis100-core.srcs/sources_1/bd/ip]
read_ip          [glob [file normalize [info script]/../../fpga/tis100-core.srcs/sources_1/bd/block_top/ip]/*/*.xci]
# read bd
read_bd          [file normalize [info script]/../../fpga/tis100-core.srcs/sources_1/bd/block_top]/block_top.bd
#read constraints
read_xdc         [glob [file normalize [info script]/../../fpga/tis100-core.srcs/constrs_1/new]/*.xdc]

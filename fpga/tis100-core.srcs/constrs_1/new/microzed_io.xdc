# ----------------------------------------------------------------------------
# User LEDs - Bank 34
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN U14 [get_ports {LED[0]}];  # "U14.JX1_LVDS_10_P.JX1.41.LED0"
set_property PACKAGE_PIN U15 [get_ports {LED[1]}];  # "U15.JX1_LVDS_10_N.JX1.43.LED1"
set_property PACKAGE_PIN U18 [get_ports {LED[2]}];  # "U18.JX1_LVDS_11_P.JX1.42.LED2"
set_property PACKAGE_PIN U19 [get_ports {LED[3]}];  # "U19.JX1_LVDS_11_N.JX1.44.LED3"
set_property PACKAGE_PIN R19 [get_ports {LED[4]}];  # "R19.JX1_SE_0.JX1.9.LED4"
set_property PACKAGE_PIN V13 [get_ports {LED[5]}];  # "V13.JX1_LVDS_2_N.JX1.19.LED5"
set_property PACKAGE_PIN P14 [get_ports {LED[6]}];  # "P14.JX1_LVDS_5_P.JX1.24.LED6"
set_property PACKAGE_PIN R14 [get_ports {LED[7]}];  # "R14.JX1_LVDS_5_N.JX1.26.LED7"


# ----------------------------------------------------------------------------
# Carrier Clock Source - Bank 35
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN K17 [get_ports {io_clk100}];     # "K17.JX2_LVDS_11_P.JX2.48.BB_CLK"
set_property PACKAGE_PIN K18 [get_ports {io_clk100_en}];  # "K18.JX2_LVDS_11_N.JX2.50.BB_CLK_EN"
create_clock -name io_clk100 -period 10 [get_ports io_clk100]


# ----------------------------------------------------------------------------
# User DIP Switches - Bank 35
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN M14 [get_ports {DIP_SW[0]}];  # "M14.JX2_LVDS_22_P.JX2.87.DIP_SW0"
set_property PACKAGE_PIN M15 [get_ports {DIP_SW[1]}];  # "M15.JX2_LVDS_22_N.JX2.89.DIP_SW1"
set_property PACKAGE_PIN K16 [get_ports {DIP_SW[2]}];  # "K16.JX2_LVDS_23_P.JX2.88.DIP_SW2"
set_property PACKAGE_PIN J16 [get_ports {DIP_SW[3]}];  # "J16.JX2_LVDS_23_N.JX2.90.DIP_SW3"


# ----------------------------------------------------------------------------
# User Push Buttons - Bank 35
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN G19 [get_ports {PB[0]}];  # "G19.JX2_LVDS_16_P.JX2.67.PB0"
set_property PACKAGE_PIN G20 [get_ports {PB[1]}];  # "G20.JX2_LVDS_16_N.JX2.69.PB1"
set_property PACKAGE_PIN J20 [get_ports {PB[2]}];  # "J20.JX2_LVDS_17_P.JX2.68.PB2"
set_property PACKAGE_PIN H20 [get_ports {PB[3]}];  # "H20.JX2_LVDS_17_N.JX2.70.PB3"


# ----------------------------------------------------------------------------
# IOSTANDARD Constraints
# ----------------------------------------------------------------------------
# Set the bank voltage for IO Bank 34 to 3.3V by default.
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 34]];
# Set the bank voltage for IO Bank 35 to 3.3V by default.
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
# Set the bank voltage for IO Bank 13 to 3.3V by default. (I/O bank available on Z7020 device only)
#set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 13]];

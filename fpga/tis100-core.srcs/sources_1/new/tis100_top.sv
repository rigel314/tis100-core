`include "types.svh"

module tis100_top (
	(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLOCK, ASSOCIATED_RESET RESETN, ASSOCIATED_BUSIF S_AXI_LITE:M_AXIS_MM2S:S_AXIS_S2MM" *)
	(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLOCK CLK" *)
	input wire clock,

	(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RESETN, POLARITY ACTIVE_LOW" *)
	(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RESETN RST" *)
	input wire resetn,

	// axi stream fifo stuff
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TDATA"  *) input  wire [31:0] m_axis_mm2s_tdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TKEEP"  *) input  wire [ 3:0] m_axis_mm2s_tkeep,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TLAST"  *) input  wire        m_axis_mm2s_tlast,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TREADY" *) output wire        m_axis_mm2s_tready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TVALID" *) input  wire        m_axis_mm2s_tvalid,

	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TDATA"  *) output wire [31:0] s_axis_s2mm_tdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TKEEP"  *) output wire [ 3:0] s_axis_s2mm_tkeep,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TLAST"  *) output wire        s_axis_s2mm_tlast,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TREADY" *) input  wire        s_axis_s2mm_tready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TVALID" *) output wire        s_axis_s2mm_tvalid,

	// axi register stuff
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARVALID" *) input  wire        s_axi_lite_arvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARREADY" *) output wire        s_axi_lite_arready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARADDR"  *) input  wire [ 9:0] s_axi_lite_araddr,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARPROT"  *) input  wire [ 2:0] s_axi_lite_arprot,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWVALID" *) input  wire        s_axi_lite_awvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWREADY" *) output wire        s_axi_lite_awready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWADDR"  *) input  wire [ 9:0] s_axi_lite_awaddr,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWPROT"  *) input  wire [ 2:0] s_axi_lite_awprot,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RVALID"  *) output wire        s_axi_lite_rvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RREADY"  *) input  wire        s_axi_lite_rready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA"   *) output wire [31:0] s_axi_lite_rdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RRESP"   *) output wire [ 1:0] s_axi_lite_rresp,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WVALID"  *) input  wire        s_axi_lite_wvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WREADY"  *) output wire        s_axi_lite_wready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WDATA"   *) input  wire [31:0] s_axi_lite_wdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WSTRB"   *) input  wire [ 3:0] s_axi_lite_wstrb,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BVALID"  *) output wire        s_axi_lite_bvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BREADY"  *) input  wire        s_axi_lite_bready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BRESP"   *) output wire [ 1:0] s_axi_lite_bresp,

	// io card stuff
	output wire        io_clk100_en,
	input  wire        io_clk100,
	input  wire [ 3:0] pb,
	input  wire [ 3:0] dip,
	output wire [ 7:0] led
);

assign io_clk100_en = '1;

led_game game(
	.clk(io_clk100),
	.rst_n(resetn),
	.pb(pb),
	.dip(dip),
	.led(led)
);

// clk and rst_n aliases for dffs to use .*
wire clk;
wire rst_n;
assign clk = clock;
assign rst_n = resetn;

// loopback dma fifo
assign s_axis_s2mm_tdata  = m_axis_mm2s_tdata;
assign s_axis_s2mm_tkeep  = m_axis_mm2s_tkeep;
assign s_axis_s2mm_tlast  = m_axis_mm2s_tlast;
assign m_axis_mm2s_tready = s_axis_s2mm_tready;
assign s_axis_s2mm_tvalid = m_axis_mm2s_tvalid;

dffro #(.T(logic       )           ) arvalid (.sig(s_axi_lite_arvalid), .*);
dffwo #(.T(logic       ), .INIT('1)) arready (.sig(s_axi_lite_arready), .*);
dffro #(.T(logic [ 9:0])           ) araddr  (.sig(s_axi_lite_araddr ), .*);
dffwo #(.T(logic       )           ) rvalid  (.sig(s_axi_lite_rvalid ), .*);
dffro #(.T(logic       )           ) rready  (.sig(s_axi_lite_rready ), .*);
dffwo #(.T(logic [31:0])           ) rdata   (.sig(s_axi_lite_rdata  ), .*);
dffwo #(.T(logic [ 1:0])           ) rresp   (.sig(s_axi_lite_rresp  ), .*);

dffrst #(.T(reg [31:0])) scratch_0 (.*);
dffrst #(.T(reg [31:0])) count     (.*);

always_comb begin
	arready.d = arready.q;
	rvalid.d  = rvalid.q;
	rdata.d   = rdata.q;
	rresp.d   = rresp.q;

	count.d = count.q + 1;

	if (arvalid.q && arready.q) begin
		arready.d = '0;
		rvalid.d  = '1;

		case (araddr.q)
			{base::ADDRESS_VER, 2'b0}: begin
				rdata.d = 32'h00_00_00_01;
				rresp.d = base::RESP_OKAY;
			end
			{base::ADDRESS_SCRATCH_0, 2'b0}: begin
				rdata.d = scratch_0.q;
				rresp.d = base::RESP_OKAY;
			end
			{base::ADDRESS_COUNT, 2'b0}: begin
				rdata.d = count.q;
				rresp.d = base::RESP_OKAY;
			end
			default: begin
				rresp.d = base::RESP_DECERR;
			end
		endcase
	end

	if (rvalid.q && rready.q) begin
		rvalid.d  = '0;
		arready.d = '1;
	end
end

dffro #(.T(logic       )           ) awvalid (.sig(s_axi_lite_awvalid), .*);
dffwo #(.T(logic       ), .INIT('1)) awready (.sig(s_axi_lite_awready), .*);
dffro #(.T(logic [ 9:0])           ) awaddr  (.sig(s_axi_lite_awaddr ), .*);
dffro #(.T(logic       )           ) wvalid  (.sig(s_axi_lite_wvalid ), .*);
dffwo #(.T(logic       )           ) wready  (.sig(s_axi_lite_wready ), .*);
dffro #(.T(logic [31:0])           ) wdata   (.sig(s_axi_lite_wdata  ), .*);
dffwo #(.T(logic       )           ) bvalid  (.sig(s_axi_lite_bvalid ), .*);
dffro #(.T(logic       )           ) bready  (.sig(s_axi_lite_bready ), .*);
dffwo #(.T(logic [ 1:0])           ) bresp   (.sig(s_axi_lite_bresp  ), .*);

dff #(.T(reg [9:0])) write_addr (.*);

always_comb begin
	awready.d = awready.q;
	wready.d  = wready.q;
	bvalid.d  = bvalid.q;
	bresp.d   = bresp.q;

	write_addr.d = write_addr.q;

	scratch_0.d = scratch_0.q;

	if (awvalid.q && awready.q) begin
		write_addr.d = awaddr.q;
		awready.d = '0;
		wready.d  = '1;
	end

	if (wvalid.q && wready.q) begin
		wready.d = '0;
		bvalid.d = '1;

		case (write_addr.q)
			{base::ADDRESS_SCRATCH_0, 2'b0}: begin
				scratch_0.d = wdata.q;
				bresp.d     = base::RESP_OKAY;
			end
			default: begin
				bresp.d = base::RESP_DECERR;
			end
		endcase
	end

	if (bvalid.q && bready.q) begin
		bvalid.d  = '0;
		awready.d = '1;
	end
end

endmodule

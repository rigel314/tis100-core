package base;
	typedef enum bit [1:0] {
		RESP_OKAY   = 2'h0,
		RESP_EXOKAY = 2'h1,
		RESP_SLVERR = 2'h2,
		RESP_DECERR = 2'h3
	} response;

	typedef enum bit [7:0] {
		ADDRESS_VER       = 'd0,
		ADDRESS_SCRATCH_0 = 'd1,
		ADDRESS_COUNT     = 'd2
	} tis100_axi_lite_address;
endpackage

interface dff #(
	parameter type T = logic [7:0]
) (
	input clk
);
	T d;
	T q;
	always_ff @(posedge clk) begin
		q <= d;
	end
endinterface // dff

interface dffrst #(
	parameter type T = logic [7:0],
	parameter T INIT = '0
) (
	input clk,
	input rst_n
);
	T d;
	T q;
	always_ff @(posedge clk or negedge rst_n) begin
		if (!rst_n) begin
			q <= INIT;
		end else begin
			q <= d;
		end
	end
endinterface // dffrst

interface dffro #(
	parameter type T = logic [7:0]
) (
	input clk,
	input wire [$bits(T)-1:0] sig
);
	T q;
	always_ff @(posedge clk) begin
		q <= sig;
	end
endinterface // dffro

interface dffwo #(
	parameter type T = logic [7:0],
	parameter T INIT = '0
) (
	input clk,
	input rst_n,
	output wire [$bits(T)-1:0] sig
);
	T d;
	T q;
	always_ff @(posedge clk or negedge rst_n) begin
		if (!rst_n) begin
			q <= INIT;
		end else begin
			q <= d;
		end
	end
	assign sig = q;
endinterface // dffwo

module tis100_top_wrap (
	(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLOCK, ASSOCIATED_RESET RESETN, ASSOCIATED_BUSIF S_AXI_LITE:M_AXIS_MM2S:S_AXIS_S2MM" *)
	(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLOCK CLK" *)
	input wire clock,

	(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RESETN, POLARITY ACTIVE_LOW" *)
	(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RESETN RST" *)
	input wire resetn,

	// axi stream fifo stuff
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TDATA"  *) input  wire [31:0] m_axis_mm2s_tdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TKEEP"  *) input  wire [ 3:0] m_axis_mm2s_tkeep,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TLAST"  *) input  wire        m_axis_mm2s_tlast,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TREADY" *) output wire        m_axis_mm2s_tready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 M_AXIS_MM2S TVALID" *) input  wire        m_axis_mm2s_tvalid,

	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TDATA"  *) output wire [31:0] s_axis_s2mm_tdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TKEEP"  *) output wire [ 3:0] s_axis_s2mm_tkeep,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TLAST"  *) output wire        s_axis_s2mm_tlast,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TREADY" *) input  wire        s_axis_s2mm_tready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:axis_rtl:1.0 S_AXIS_S2MM TVALID" *) output wire        s_axis_s2mm_tvalid,

	// axi register stuff
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARVALID" *) input  wire        s_axi_lite_arvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARREADY" *) output wire        s_axi_lite_arready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARADDR"  *) input  wire [ 9:0] s_axi_lite_araddr,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARPROT"  *) input  wire [ 2:0] s_axi_lite_arprot,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWVALID" *) input  wire        s_axi_lite_awvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWREADY" *) output wire        s_axi_lite_awready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWADDR"  *) input  wire [ 9:0] s_axi_lite_awaddr,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWPROT"  *) input  wire [ 2:0] s_axi_lite_awprot,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RVALID"  *) output wire        s_axi_lite_rvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RREADY"  *) input  wire        s_axi_lite_rready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA"   *) output wire [31:0] s_axi_lite_rdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RRESP"   *) output wire [ 1:0] s_axi_lite_rresp,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WVALID"  *) input  wire        s_axi_lite_wvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WREADY"  *) output wire        s_axi_lite_wready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WDATA"   *) input  wire [31:0] s_axi_lite_wdata,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WSTRB"   *) input  wire [ 3:0] s_axi_lite_wstrb,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BVALID"  *) output wire        s_axi_lite_bvalid,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BREADY"  *) input  wire        s_axi_lite_bready,
	(* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BRESP"   *) output wire [ 1:0] s_axi_lite_bresp,

    // io card stuff
	output wire        io_clk100_en,
	input  wire        io_clk100,
	input  wire [ 3:0] pb,
	input  wire [ 3:0] dip,
	output wire [ 7:0] led
);

tis100_top tis100 (
    .clock(clock),
    .resetn(resetn),
    .m_axis_mm2s_tdata(m_axis_mm2s_tdata),
    .m_axis_mm2s_tkeep(m_axis_mm2s_tkeep),
    .m_axis_mm2s_tlast(m_axis_mm2s_tlast),
    .m_axis_mm2s_tready(m_axis_mm2s_tready),
    .m_axis_mm2s_tvalid(m_axis_mm2s_tvalid),
    .s_axis_s2mm_tdata(s_axis_s2mm_tdata),
    .s_axis_s2mm_tkeep(s_axis_s2mm_tkeep),
    .s_axis_s2mm_tlast(s_axis_s2mm_tlast),
    .s_axis_s2mm_tready(s_axis_s2mm_tready),
    .s_axis_s2mm_tvalid(s_axis_s2mm_tvalid),
    .s_axi_lite_arvalid(s_axi_lite_arvalid),
    .s_axi_lite_arready(s_axi_lite_arready),
    .s_axi_lite_araddr(s_axi_lite_araddr),
    .s_axi_lite_arprot(s_axi_lite_arprot),
    .s_axi_lite_awvalid(s_axi_lite_awvalid),
    .s_axi_lite_awready(s_axi_lite_awready),
    .s_axi_lite_awaddr(s_axi_lite_awaddr),
    .s_axi_lite_awprot(s_axi_lite_awprot),
    .s_axi_lite_rvalid(s_axi_lite_rvalid),
    .s_axi_lite_rready(s_axi_lite_rready),
    .s_axi_lite_rdata(s_axi_lite_rdata),
    .s_axi_lite_rresp(s_axi_lite_rresp),
    .s_axi_lite_wvalid(s_axi_lite_wvalid),
    .s_axi_lite_wready(s_axi_lite_wready),
    .s_axi_lite_wdata(s_axi_lite_wdata),
    .s_axi_lite_wstrb(s_axi_lite_wstrb),
    .s_axi_lite_bvalid(s_axi_lite_bvalid),
    .s_axi_lite_bready(s_axi_lite_bready),
    .s_axi_lite_bresp(s_axi_lite_bresp),
    .io_clk100_en(io_clk100_en),
    .io_clk100(io_clk100),
    .pb(pb),
    .dip(dip),
    .led(led)
);

endmodule

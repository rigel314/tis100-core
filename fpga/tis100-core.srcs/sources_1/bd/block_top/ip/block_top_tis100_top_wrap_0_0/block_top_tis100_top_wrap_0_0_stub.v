// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Mon Nov 30 07:07:57 2020
// Host        : IRIS running 64-bit unknown
// Command     : write_verilog -force -mode synth_stub
//               /mnt/c/Users/test/source/tis100-core/fpga/tis100-core.srcs/sources_1/bd/block_top/ip/block_top_tis100_top_wrap_0_0/block_top_tis100_top_wrap_0_0_stub.v
// Design      : block_top_tis100_top_wrap_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "tis100_top_wrap,Vivado 2020.1" *)
module block_top_tis100_top_wrap_0_0(clock, resetn, m_axis_mm2s_tdata, 
  m_axis_mm2s_tkeep, m_axis_mm2s_tlast, m_axis_mm2s_tready, m_axis_mm2s_tvalid, 
  s_axis_s2mm_tdata, s_axis_s2mm_tkeep, s_axis_s2mm_tlast, s_axis_s2mm_tready, 
  s_axis_s2mm_tvalid, s_axi_lite_arvalid, s_axi_lite_arready, s_axi_lite_araddr, 
  s_axi_lite_arprot, s_axi_lite_awvalid, s_axi_lite_awready, s_axi_lite_awaddr, 
  s_axi_lite_awprot, s_axi_lite_rvalid, s_axi_lite_rready, s_axi_lite_rdata, 
  s_axi_lite_rresp, s_axi_lite_wvalid, s_axi_lite_wready, s_axi_lite_wdata, 
  s_axi_lite_wstrb, s_axi_lite_bvalid, s_axi_lite_bready, s_axi_lite_bresp, io_clk100_en, 
  io_clk100, pb, dip, led)
/* synthesis syn_black_box black_box_pad_pin="clock,resetn,m_axis_mm2s_tdata[31:0],m_axis_mm2s_tkeep[3:0],m_axis_mm2s_tlast,m_axis_mm2s_tready,m_axis_mm2s_tvalid,s_axis_s2mm_tdata[31:0],s_axis_s2mm_tkeep[3:0],s_axis_s2mm_tlast,s_axis_s2mm_tready,s_axis_s2mm_tvalid,s_axi_lite_arvalid,s_axi_lite_arready,s_axi_lite_araddr[9:0],s_axi_lite_arprot[2:0],s_axi_lite_awvalid,s_axi_lite_awready,s_axi_lite_awaddr[9:0],s_axi_lite_awprot[2:0],s_axi_lite_rvalid,s_axi_lite_rready,s_axi_lite_rdata[31:0],s_axi_lite_rresp[1:0],s_axi_lite_wvalid,s_axi_lite_wready,s_axi_lite_wdata[31:0],s_axi_lite_wstrb[3:0],s_axi_lite_bvalid,s_axi_lite_bready,s_axi_lite_bresp[1:0],io_clk100_en,io_clk100,pb[3:0],dip[3:0],led[7:0]" */;
  input clock;
  input resetn;
  input [31:0]m_axis_mm2s_tdata;
  input [3:0]m_axis_mm2s_tkeep;
  input m_axis_mm2s_tlast;
  output m_axis_mm2s_tready;
  input m_axis_mm2s_tvalid;
  output [31:0]s_axis_s2mm_tdata;
  output [3:0]s_axis_s2mm_tkeep;
  output s_axis_s2mm_tlast;
  input s_axis_s2mm_tready;
  output s_axis_s2mm_tvalid;
  input s_axi_lite_arvalid;
  output s_axi_lite_arready;
  input [9:0]s_axi_lite_araddr;
  input [2:0]s_axi_lite_arprot;
  input s_axi_lite_awvalid;
  output s_axi_lite_awready;
  input [9:0]s_axi_lite_awaddr;
  input [2:0]s_axi_lite_awprot;
  output s_axi_lite_rvalid;
  input s_axi_lite_rready;
  output [31:0]s_axi_lite_rdata;
  output [1:0]s_axi_lite_rresp;
  input s_axi_lite_wvalid;
  output s_axi_lite_wready;
  input [31:0]s_axi_lite_wdata;
  input [3:0]s_axi_lite_wstrb;
  output s_axi_lite_bvalid;
  input s_axi_lite_bready;
  output [1:0]s_axi_lite_bresp;
  output io_clk100_en;
  input io_clk100;
  input [3:0]pb;
  input [3:0]dip;
  output [7:0]led;
endmodule

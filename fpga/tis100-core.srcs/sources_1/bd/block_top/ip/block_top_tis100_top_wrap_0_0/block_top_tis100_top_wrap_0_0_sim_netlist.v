// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
// Date        : Mon Nov 30 07:07:57 2020
// Host        : IRIS running 64-bit unknown
// Command     : write_verilog -force -mode funcsim
//               /mnt/c/Users/test/source/tis100-core/fpga/tis100-core.srcs/sources_1/bd/block_top/ip/block_top_tis100_top_wrap_0_0/block_top_tis100_top_wrap_0_0_sim_netlist.v
// Design      : block_top_tis100_top_wrap_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "block_top_tis100_top_wrap_0_0,tis100_top_wrap,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "tis100_top_wrap,Vivado 2020.1" *) 
(* NotValidForBitStream *)
module block_top_tis100_top_wrap_0_0
   (clock,
    resetn,
    m_axis_mm2s_tdata,
    m_axis_mm2s_tkeep,
    m_axis_mm2s_tlast,
    m_axis_mm2s_tready,
    m_axis_mm2s_tvalid,
    s_axis_s2mm_tdata,
    s_axis_s2mm_tkeep,
    s_axis_s2mm_tlast,
    s_axis_s2mm_tready,
    s_axis_s2mm_tvalid,
    s_axi_lite_arvalid,
    s_axi_lite_arready,
    s_axi_lite_araddr,
    s_axi_lite_arprot,
    s_axi_lite_awvalid,
    s_axi_lite_awready,
    s_axi_lite_awaddr,
    s_axi_lite_awprot,
    s_axi_lite_rvalid,
    s_axi_lite_rready,
    s_axi_lite_rdata,
    s_axi_lite_rresp,
    s_axi_lite_wvalid,
    s_axi_lite_wready,
    s_axi_lite_wdata,
    s_axi_lite_wstrb,
    s_axi_lite_bvalid,
    s_axi_lite_bready,
    s_axi_lite_bresp,
    io_clk100_en,
    io_clk100,
    pb,
    dip,
    led);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLOCK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLOCK, ASSOCIATED_BUSIF S_AXI_LITE:M_AXIS_MM2S:S_AXIS_S2MM, ASSOCIATED_RESET RESETN, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN block_top_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input clock;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input resetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_MM2S TDATA" *) input [31:0]m_axis_mm2s_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_MM2S TKEEP" *) input [3:0]m_axis_mm2s_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_MM2S TLAST" *) input m_axis_mm2s_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_MM2S TREADY" *) output m_axis_mm2s_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_MM2S TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXIS_MM2S, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_top_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m_axis_mm2s_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_S2MM TDATA" *) output [31:0]s_axis_s2mm_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_S2MM TKEEP" *) output [3:0]s_axis_s2mm_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_S2MM TLAST" *) output s_axis_s2mm_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_S2MM TREADY" *) input s_axis_s2mm_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 S_AXIS_S2MM TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXIS_S2MM, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN block_top_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) output s_axis_s2mm_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE ARVALID" *) input s_axi_lite_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE ARREADY" *) output s_axi_lite_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE ARADDR" *) input [9:0]s_axi_lite_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE ARPROT" *) input [2:0]s_axi_lite_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE AWVALID" *) input s_axi_lite_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE AWREADY" *) output s_axi_lite_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE AWADDR" *) input [9:0]s_axi_lite_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE AWPROT" *) input [2:0]s_axi_lite_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE RVALID" *) output s_axi_lite_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE RREADY" *) input s_axi_lite_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE RDATA" *) output [31:0]s_axi_lite_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE RRESP" *) output [1:0]s_axi_lite_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE WVALID" *) input s_axi_lite_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE WREADY" *) output s_axi_lite_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE WDATA" *) input [31:0]s_axi_lite_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE WSTRB" *) input [3:0]s_axi_lite_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE BVALID" *) output s_axi_lite_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE BREADY" *) input s_axi_lite_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI_LITE BRESP" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXI_LITE, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 10, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN block_top_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output [1:0]s_axi_lite_bresp;
  output io_clk100_en;
  input io_clk100;
  input [3:0]pb;
  input [3:0]dip;
  output [7:0]led;

  wire \<const1> ;
  wire clock;
  wire [3:0]dip;
  wire io_clk100;
  wire [7:0]led;
  wire [31:0]m_axis_mm2s_tdata;
  wire [3:0]m_axis_mm2s_tkeep;
  wire m_axis_mm2s_tlast;
  wire m_axis_mm2s_tvalid;
  wire [3:0]pb;
  wire resetn;
  wire [9:0]s_axi_lite_araddr;
  wire s_axi_lite_arready;
  wire s_axi_lite_arvalid;
  wire [9:0]s_axi_lite_awaddr;
  wire s_axi_lite_awready;
  wire s_axi_lite_awvalid;
  wire s_axi_lite_bready;
  wire [0:0]\^s_axi_lite_bresp ;
  wire s_axi_lite_bvalid;
  wire [31:0]s_axi_lite_rdata;
  wire s_axi_lite_rready;
  wire [0:0]\^s_axi_lite_rresp ;
  wire s_axi_lite_rvalid;
  wire [31:0]s_axi_lite_wdata;
  wire s_axi_lite_wready;
  wire s_axi_lite_wvalid;
  wire s_axis_s2mm_tready;

  assign io_clk100_en = \<const1> ;
  assign m_axis_mm2s_tready = s_axis_s2mm_tready;
  assign s_axi_lite_bresp[1] = \^s_axi_lite_bresp [0];
  assign s_axi_lite_bresp[0] = \^s_axi_lite_bresp [0];
  assign s_axi_lite_rresp[1] = \^s_axi_lite_rresp [0];
  assign s_axi_lite_rresp[0] = \^s_axi_lite_rresp [0];
  assign s_axis_s2mm_tdata[31:0] = m_axis_mm2s_tdata;
  assign s_axis_s2mm_tkeep[3:0] = m_axis_mm2s_tkeep;
  assign s_axis_s2mm_tlast = m_axis_mm2s_tlast;
  assign s_axis_s2mm_tvalid = m_axis_mm2s_tvalid;
  VCC VCC
       (.P(\<const1> ));
  block_top_tis100_top_wrap_0_0_tis100_top_wrap inst
       (.clock(clock),
        .dip(dip),
        .io_clk100(io_clk100),
        .led(led[7:4]),
        .pb(pb),
        .\q_reg[0]__1 (led[0]),
        .\q_reg[1]__1 (led[1]),
        .\q_reg[2]__0 (led[2]),
        .\q_reg[3]__0 (led[3]),
        .q_reg__2(s_axi_lite_arready),
        .q_reg__5(s_axi_lite_wready),
        .q_reg__8(s_axi_lite_awready),
        .resetn(resetn),
        .s_axi_lite_araddr(s_axi_lite_araddr),
        .s_axi_lite_arvalid(s_axi_lite_arvalid),
        .s_axi_lite_awaddr(s_axi_lite_awaddr),
        .s_axi_lite_awvalid(s_axi_lite_awvalid),
        .s_axi_lite_bready(s_axi_lite_bready),
        .s_axi_lite_bresp(\^s_axi_lite_bresp ),
        .s_axi_lite_bvalid(s_axi_lite_bvalid),
        .s_axi_lite_rdata(s_axi_lite_rdata),
        .s_axi_lite_rready(s_axi_lite_rready),
        .s_axi_lite_rresp(\^s_axi_lite_rresp ),
        .s_axi_lite_rvalid(s_axi_lite_rvalid),
        .s_axi_lite_wdata(s_axi_lite_wdata),
        .s_axi_lite_wvalid(s_axi_lite_wvalid));
endmodule

(* ORIG_REF_NAME = "led_game" *) 
module block_top_tis100_top_wrap_0_0_led_game
   (led,
    \q_reg[3]__0_0 ,
    \q_reg[2]__0_0 ,
    \q_reg[1]__1_0 ,
    \q_reg[0]__1_0 ,
    io_clk100,
    \q_reg[31]__0_0 ,
    pb,
    dip);
  output [3:0]led;
  output \q_reg[3]__0_0 ;
  output \q_reg[2]__0_0 ;
  output \q_reg[1]__1_0 ;
  output \q_reg[0]__1_0 ;
  input io_clk100;
  input \q_reg[31]__0_0 ;
  input [3:0]pb;
  input [3:0]dip;

  wire \FSM_sequential_q[0]_i_1_n_0 ;
  wire \FSM_sequential_q[0]_i_3_n_0 ;
  wire \FSM_sequential_q[0]_i_4_n_0 ;
  wire \FSM_sequential_q[0]_i_5_n_0 ;
  wire \FSM_sequential_q[0]_i_6_n_0 ;
  wire \FSM_sequential_q[0]_i_7_n_0 ;
  wire \FSM_sequential_q[0]_i_8_n_0 ;
  wire \FSM_sequential_q[0]_i_9_n_0 ;
  wire \FSM_sequential_q[1]_i_10_n_0 ;
  wire \FSM_sequential_q[1]_i_11_n_0 ;
  wire \FSM_sequential_q[1]_i_12_n_0 ;
  wire \FSM_sequential_q[1]_i_13_n_0 ;
  wire \FSM_sequential_q[1]_i_14_n_0 ;
  wire \FSM_sequential_q[1]_i_15_n_0 ;
  wire \FSM_sequential_q[1]_i_16_n_0 ;
  wire \FSM_sequential_q[1]_i_17_n_0 ;
  wire \FSM_sequential_q[1]_i_18_n_0 ;
  wire \FSM_sequential_q[1]_i_19_n_0 ;
  wire \FSM_sequential_q[1]_i_1_n_0 ;
  wire \FSM_sequential_q[1]_i_20_n_0 ;
  wire \FSM_sequential_q[1]_i_21_n_0 ;
  wire \FSM_sequential_q[1]_i_22_n_0 ;
  wire \FSM_sequential_q[1]_i_2_n_0 ;
  wire \FSM_sequential_q[1]_i_3_n_0 ;
  wire \FSM_sequential_q[1]_i_4_n_0 ;
  wire \FSM_sequential_q[1]_i_5_n_0 ;
  wire \FSM_sequential_q[1]_i_6_n_0 ;
  wire \FSM_sequential_q[1]_i_7_n_0 ;
  wire \FSM_sequential_q[1]_i_8_n_0 ;
  wire \FSM_sequential_q[1]_i_9_n_0 ;
  wire d0_carry__0_n_0;
  wire d0_carry__0_n_1;
  wire d0_carry__0_n_2;
  wire d0_carry__0_n_3;
  wire d0_carry__0_n_4;
  wire d0_carry__0_n_5;
  wire d0_carry__0_n_6;
  wire d0_carry__0_n_7;
  wire d0_carry__1_n_0;
  wire d0_carry__1_n_1;
  wire d0_carry__1_n_2;
  wire d0_carry__1_n_3;
  wire d0_carry__1_n_4;
  wire d0_carry__1_n_5;
  wire d0_carry__1_n_6;
  wire d0_carry__1_n_7;
  wire d0_carry__2_n_0;
  wire d0_carry__2_n_1;
  wire d0_carry__2_n_2;
  wire d0_carry__2_n_3;
  wire d0_carry__2_n_4;
  wire d0_carry__2_n_5;
  wire d0_carry__2_n_6;
  wire d0_carry__2_n_7;
  wire d0_carry__3_n_0;
  wire d0_carry__3_n_1;
  wire d0_carry__3_n_2;
  wire d0_carry__3_n_3;
  wire d0_carry__3_n_4;
  wire d0_carry__3_n_5;
  wire d0_carry__3_n_6;
  wire d0_carry__3_n_7;
  wire d0_carry__4_n_0;
  wire d0_carry__4_n_1;
  wire d0_carry__4_n_2;
  wire d0_carry__4_n_3;
  wire d0_carry__4_n_4;
  wire d0_carry__4_n_5;
  wire d0_carry__4_n_6;
  wire d0_carry__4_n_7;
  wire d0_carry__5_n_0;
  wire d0_carry__5_n_1;
  wire d0_carry__5_n_2;
  wire d0_carry__5_n_3;
  wire d0_carry__5_n_4;
  wire d0_carry__5_n_5;
  wire d0_carry__5_n_6;
  wire d0_carry__5_n_7;
  wire d0_carry__6_n_2;
  wire d0_carry__6_n_3;
  wire d0_carry__6_n_5;
  wire d0_carry__6_n_6;
  wire d0_carry__6_n_7;
  wire d0_carry_n_0;
  wire d0_carry_n_1;
  wire d0_carry_n_2;
  wire d0_carry_n_3;
  wire d0_carry_n_4;
  wire d0_carry_n_5;
  wire d0_carry_n_6;
  wire d0_carry_n_7;
  wire [0:0]d__0;
  wire [3:0]dip;
  wire io_clk100;
  wire [3:0]led;
  wire p_0_in0;
  wire [10:1]p_1_out;
  wire [10:1]p_3_out;
  wire [10:1]p_5_out;
  wire [10:1]p_7_out;
  wire [3:0]pb;
  wire \pb_shift_reg_n_0_[0][10] ;
  wire \pb_shift_reg_n_0_[1][10] ;
  wire \pb_shift_reg_n_0_[2][10] ;
  wire \pb_shift_reg_n_0_[3][10] ;
  wire \q[0]__0_i_1_n_0 ;
  wire \q[0]__1_i_10_n_0 ;
  wire \q[0]__1_i_11_n_0 ;
  wire \q[0]__1_i_12_n_0 ;
  wire \q[0]__1_i_13_n_0 ;
  wire \q[0]__1_i_1_n_0 ;
  wire \q[0]__1_i_2_n_0 ;
  wire \q[0]__1_i_3_n_0 ;
  wire \q[0]__1_i_4_n_0 ;
  wire \q[0]__1_i_5_n_0 ;
  wire \q[0]__1_i_6_n_0 ;
  wire \q[0]__1_i_7_n_0 ;
  wire \q[0]__1_i_8_n_0 ;
  wire \q[0]__1_i_9_n_0 ;
  wire \q[0]_i_1_n_0 ;
  wire \q[0]_i_2_n_0 ;
  wire \q[10]_i_1_n_0 ;
  wire \q[11]_i_1_n_0 ;
  wire \q[12]_i_1_n_0 ;
  wire \q[13]_i_1_n_0 ;
  wire \q[14]_i_1_n_0 ;
  wire \q[15]_i_1_n_0 ;
  wire \q[16]_i_1_n_0 ;
  wire \q[17]_i_1_n_0 ;
  wire \q[18]_i_1_n_0 ;
  wire \q[19]_i_1_n_0 ;
  wire \q[1]__0_i_1_n_0 ;
  wire \q[1]__1_i_10_n_0 ;
  wire \q[1]__1_i_11_n_0 ;
  wire \q[1]__1_i_12_n_0 ;
  wire \q[1]__1_i_1_n_0 ;
  wire \q[1]__1_i_2_n_0 ;
  wire \q[1]__1_i_3_n_0 ;
  wire \q[1]__1_i_4_n_0 ;
  wire \q[1]__1_i_5_n_0 ;
  wire \q[1]__1_i_6_n_0 ;
  wire \q[1]__1_i_7_n_0 ;
  wire \q[1]__1_i_8_n_0 ;
  wire \q[1]__1_i_9_n_0 ;
  wire \q[1]_i_1_n_0 ;
  wire \q[20]_i_1_n_0 ;
  wire \q[21]_i_1_n_0 ;
  wire \q[22]_i_1_n_0 ;
  wire \q[23]_i_1_n_0 ;
  wire \q[24]_i_1_n_0 ;
  wire \q[25]_i_1_n_0 ;
  wire \q[26]_i_1_n_0 ;
  wire \q[27]_i_1_n_0 ;
  wire \q[28]_i_1_n_0 ;
  wire \q[29]_i_1_n_0 ;
  wire \q[2]__0_i_10_n_0 ;
  wire \q[2]__0_i_11_n_0 ;
  wire \q[2]__0_i_12_n_0 ;
  wire \q[2]__0_i_13_n_0 ;
  wire \q[2]__0_i_14_n_0 ;
  wire \q[2]__0_i_1_n_0 ;
  wire \q[2]__0_i_2_n_0 ;
  wire \q[2]__0_i_3_n_0 ;
  wire \q[2]__0_i_4_n_0 ;
  wire \q[2]__0_i_5_n_0 ;
  wire \q[2]__0_i_6_n_0 ;
  wire \q[2]__0_i_7_n_0 ;
  wire \q[2]__0_i_8_n_0 ;
  wire \q[2]__0_i_9_n_0 ;
  wire \q[2]_i_1_n_0 ;
  wire \q[30]_i_1_n_0 ;
  wire \q[31]_i_10_n_0 ;
  wire \q[31]_i_11_n_0 ;
  wire \q[31]_i_12_n_0 ;
  wire \q[31]_i_13_n_0 ;
  wire \q[31]_i_14_n_0 ;
  wire \q[31]_i_15_n_0 ;
  wire \q[31]_i_16_n_0 ;
  wire \q[31]_i_17_n_0 ;
  wire \q[31]_i_18_n_0 ;
  wire \q[31]_i_19_n_0 ;
  wire \q[31]_i_1_n_0 ;
  wire \q[31]_i_20_n_0 ;
  wire \q[31]_i_21_n_0 ;
  wire \q[31]_i_22_n_0 ;
  wire \q[31]_i_2_n_0 ;
  wire \q[31]_i_3_n_0 ;
  wire \q[31]_i_4_n_0 ;
  wire \q[31]_i_5_n_0 ;
  wire \q[31]_i_6_n_0 ;
  wire \q[31]_i_7_n_0 ;
  wire \q[31]_i_8_n_0 ;
  wire \q[31]_i_9_n_0 ;
  wire \q[3]__0_i_10_n_0 ;
  wire \q[3]__0_i_11_n_0 ;
  wire \q[3]__0_i_12_n_0 ;
  wire \q[3]__0_i_13_n_0 ;
  wire \q[3]__0_i_14_n_0 ;
  wire \q[3]__0_i_15_n_0 ;
  wire \q[3]__0_i_16_n_0 ;
  wire \q[3]__0_i_17_n_0 ;
  wire \q[3]__0_i_18_n_0 ;
  wire \q[3]__0_i_19_n_0 ;
  wire \q[3]__0_i_1_n_0 ;
  wire \q[3]__0_i_20_n_0 ;
  wire \q[3]__0_i_21_n_0 ;
  wire \q[3]__0_i_22_n_0 ;
  wire \q[3]__0_i_23_n_0 ;
  wire \q[3]__0_i_24_n_0 ;
  wire \q[3]__0_i_25_n_0 ;
  wire \q[3]__0_i_26_n_0 ;
  wire \q[3]__0_i_27_n_0 ;
  wire \q[3]__0_i_28_n_0 ;
  wire \q[3]__0_i_29_n_0 ;
  wire \q[3]__0_i_2_n_0 ;
  wire \q[3]__0_i_30_n_0 ;
  wire \q[3]__0_i_31_n_0 ;
  wire \q[3]__0_i_32_n_0 ;
  wire \q[3]__0_i_33_n_0 ;
  wire \q[3]__0_i_34_n_0 ;
  wire \q[3]__0_i_35_n_0 ;
  wire \q[3]__0_i_36_n_0 ;
  wire \q[3]__0_i_37_n_0 ;
  wire \q[3]__0_i_38_n_0 ;
  wire \q[3]__0_i_39_n_0 ;
  wire \q[3]__0_i_3_n_0 ;
  wire \q[3]__0_i_40_n_0 ;
  wire \q[3]__0_i_41_n_0 ;
  wire \q[3]__0_i_42_n_0 ;
  wire \q[3]__0_i_4_n_0 ;
  wire \q[3]__0_i_5_n_0 ;
  wire \q[3]__0_i_6_n_0 ;
  wire \q[3]__0_i_7_n_0 ;
  wire \q[3]__0_i_8_n_0 ;
  wire \q[3]__0_i_9_n_0 ;
  wire \q[3]_i_1_n_0 ;
  wire \q[4]_i_1_n_0 ;
  wire \q[5]_i_1_n_0 ;
  wire \q[6]_i_1_n_0 ;
  wire \q[7]_i_1_n_0 ;
  wire \q[8]_i_1_n_0 ;
  wire \q[9]_i_1_n_0 ;
  wire [1:0]q_reg;
  wire \q_reg[0]__0_n_0 ;
  wire \q_reg[0]__1_0 ;
  wire \q_reg[0]__2_n_0 ;
  wire \q_reg[0]_i_1_n_0 ;
  wire \q_reg[0]_i_1_n_1 ;
  wire \q_reg[0]_i_1_n_2 ;
  wire \q_reg[0]_i_1_n_3 ;
  wire \q_reg[0]_i_1_n_4 ;
  wire \q_reg[0]_i_1_n_5 ;
  wire \q_reg[0]_i_1_n_6 ;
  wire \q_reg[0]_i_1_n_7 ;
  wire \q_reg[10]__0_n_0 ;
  wire \q_reg[11]__0_n_0 ;
  wire \q_reg[12]__0_n_0 ;
  wire \q_reg[12]_i_1_n_0 ;
  wire \q_reg[12]_i_1_n_1 ;
  wire \q_reg[12]_i_1_n_2 ;
  wire \q_reg[12]_i_1_n_3 ;
  wire \q_reg[12]_i_1_n_4 ;
  wire \q_reg[12]_i_1_n_5 ;
  wire \q_reg[12]_i_1_n_6 ;
  wire \q_reg[12]_i_1_n_7 ;
  wire \q_reg[13]__0_n_0 ;
  wire \q_reg[14]__0_n_0 ;
  wire \q_reg[15]__0_n_0 ;
  wire \q_reg[16]__0_n_0 ;
  wire \q_reg[16]_i_1_n_0 ;
  wire \q_reg[16]_i_1_n_1 ;
  wire \q_reg[16]_i_1_n_2 ;
  wire \q_reg[16]_i_1_n_3 ;
  wire \q_reg[16]_i_1_n_4 ;
  wire \q_reg[16]_i_1_n_5 ;
  wire \q_reg[16]_i_1_n_6 ;
  wire \q_reg[16]_i_1_n_7 ;
  wire \q_reg[17]__0_n_0 ;
  wire \q_reg[18]__0_n_0 ;
  wire \q_reg[19]__0_n_0 ;
  wire \q_reg[1]__0_n_0 ;
  wire \q_reg[1]__1_0 ;
  wire \q_reg[1]__2_n_0 ;
  wire \q_reg[20]__0_n_0 ;
  wire \q_reg[20]_i_1_n_0 ;
  wire \q_reg[20]_i_1_n_1 ;
  wire \q_reg[20]_i_1_n_2 ;
  wire \q_reg[20]_i_1_n_3 ;
  wire \q_reg[20]_i_1_n_4 ;
  wire \q_reg[20]_i_1_n_5 ;
  wire \q_reg[20]_i_1_n_6 ;
  wire \q_reg[20]_i_1_n_7 ;
  wire \q_reg[21]__0_n_0 ;
  wire \q_reg[22]__0_n_0 ;
  wire \q_reg[23]__0_n_0 ;
  wire \q_reg[24]__0_n_0 ;
  wire \q_reg[24]_i_1_n_0 ;
  wire \q_reg[24]_i_1_n_1 ;
  wire \q_reg[24]_i_1_n_2 ;
  wire \q_reg[24]_i_1_n_3 ;
  wire \q_reg[24]_i_1_n_4 ;
  wire \q_reg[24]_i_1_n_5 ;
  wire \q_reg[24]_i_1_n_6 ;
  wire \q_reg[24]_i_1_n_7 ;
  wire \q_reg[25]__0_n_0 ;
  wire \q_reg[26]__0_n_0 ;
  wire \q_reg[27]__0_n_0 ;
  wire \q_reg[2]__0_0 ;
  wire \q_reg[2]__1_n_0 ;
  wire \q_reg[31]__0_0 ;
  wire \q_reg[31]_i_1_n_1 ;
  wire \q_reg[31]_i_1_n_2 ;
  wire \q_reg[31]_i_1_n_3 ;
  wire \q_reg[31]_i_1_n_4 ;
  wire \q_reg[31]_i_1_n_5 ;
  wire \q_reg[31]_i_1_n_6 ;
  wire \q_reg[31]_i_1_n_7 ;
  wire \q_reg[3]__0_0 ;
  wire \q_reg[3]__1_n_0 ;
  wire \q_reg[4]__0_n_0 ;
  wire \q_reg[4]_i_1_n_0 ;
  wire \q_reg[4]_i_1_n_1 ;
  wire \q_reg[4]_i_1_n_2 ;
  wire \q_reg[4]_i_1_n_3 ;
  wire \q_reg[4]_i_1_n_4 ;
  wire \q_reg[4]_i_1_n_5 ;
  wire \q_reg[4]_i_1_n_6 ;
  wire \q_reg[4]_i_1_n_7 ;
  wire \q_reg[5]__0_n_0 ;
  wire \q_reg[6]__0_n_0 ;
  wire \q_reg[7]__0_n_0 ;
  wire \q_reg[8]__0_n_0 ;
  wire \q_reg[8]_i_1_n_0 ;
  wire \q_reg[8]_i_1_n_1 ;
  wire \q_reg[8]_i_1_n_2 ;
  wire \q_reg[8]_i_1_n_3 ;
  wire \q_reg[8]_i_1_n_4 ;
  wire \q_reg[8]_i_1_n_5 ;
  wire \q_reg[8]_i_1_n_6 ;
  wire \q_reg[8]_i_1_n_7 ;
  wire \q_reg[9]__0_n_0 ;
  wire \q_reg_n_0_[0] ;
  wire \q_reg_n_0_[10] ;
  wire \q_reg_n_0_[11] ;
  wire \q_reg_n_0_[12] ;
  wire \q_reg_n_0_[13] ;
  wire \q_reg_n_0_[14] ;
  wire \q_reg_n_0_[15] ;
  wire \q_reg_n_0_[16] ;
  wire \q_reg_n_0_[17] ;
  wire \q_reg_n_0_[18] ;
  wire \q_reg_n_0_[19] ;
  wire \q_reg_n_0_[1] ;
  wire \q_reg_n_0_[20] ;
  wire \q_reg_n_0_[21] ;
  wire \q_reg_n_0_[22] ;
  wire \q_reg_n_0_[23] ;
  wire \q_reg_n_0_[24] ;
  wire \q_reg_n_0_[25] ;
  wire \q_reg_n_0_[26] ;
  wire \q_reg_n_0_[27] ;
  wire \q_reg_n_0_[29] ;
  wire \q_reg_n_0_[2] ;
  wire \q_reg_n_0_[30] ;
  wire \q_reg_n_0_[31] ;
  wire \q_reg_n_0_[3] ;
  wire \q_reg_n_0_[4] ;
  wire \q_reg_n_0_[5] ;
  wire \q_reg_n_0_[6] ;
  wire \q_reg_n_0_[7] ;
  wire \q_reg_n_0_[8] ;
  wire \q_reg_n_0_[9] ;
  wire [3:2]NLW_d0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_d0_carry__6_O_UNCONNECTED;
  wire [3:3]\NLW_q_reg[31]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFF00FF00CCC8DDC8)) 
    \FSM_sequential_q[0]_i_1 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\FSM_sequential_q[1]_i_2_n_0 ),
        .I3(d__0),
        .I4(\FSM_sequential_q[0]_i_3_n_0 ),
        .I5(\FSM_sequential_q[1]_i_4_n_0 ),
        .O(\FSM_sequential_q[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h31111111)) 
    \FSM_sequential_q[0]_i_2 
       (.I0(q_reg[0]),
        .I1(q_reg[1]),
        .I2(\q_reg_n_0_[0] ),
        .I3(\q_reg_n_0_[1] ),
        .I4(\FSM_sequential_q[0]_i_4_n_0 ),
        .O(d__0));
  LUT5 #(
    .INIT(32'hFFFFFF7F)) 
    \FSM_sequential_q[0]_i_3 
       (.I0(\FSM_sequential_q[1]_i_9_n_0 ),
        .I1(\FSM_sequential_q[0]_i_5_n_0 ),
        .I2(\FSM_sequential_q[0]_i_6_n_0 ),
        .I3(\FSM_sequential_q[0]_i_7_n_0 ),
        .I4(\FSM_sequential_q[0]_i_8_n_0 ),
        .O(\FSM_sequential_q[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF6FF6)) 
    \FSM_sequential_q[0]_i_4 
       (.I0(\q_reg[0]__1_0 ),
        .I1(dip[0]),
        .I2(\q_reg[1]__1_0 ),
        .I3(dip[1]),
        .I4(\FSM_sequential_q[0]_i_9_n_0 ),
        .O(\FSM_sequential_q[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \FSM_sequential_q[0]_i_5 
       (.I0(p_7_out[5]),
        .I1(p_5_out[5]),
        .I2(p_3_out[5]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .I5(p_1_out[5]),
        .O(\FSM_sequential_q[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAAFFF0CCAA00F0CC)) 
    \FSM_sequential_q[0]_i_6 
       (.I0(p_1_out[1]),
        .I1(p_7_out[1]),
        .I2(p_3_out[1]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .I5(p_5_out[1]),
        .O(\FSM_sequential_q[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \FSM_sequential_q[0]_i_7 
       (.I0(\FSM_sequential_q[1]_i_14_n_0 ),
        .I1(\FSM_sequential_q[1]_i_16_n_0 ),
        .I2(\FSM_sequential_q[1]_i_7_n_0 ),
        .I3(\FSM_sequential_q[1]_i_17_n_0 ),
        .O(\FSM_sequential_q[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \FSM_sequential_q[0]_i_8 
       (.I0(\FSM_sequential_q[1]_i_15_n_0 ),
        .I1(\FSM_sequential_q[1]_i_8_n_0 ),
        .I2(\FSM_sequential_q[1]_i_5_n_0 ),
        .I3(\FSM_sequential_q[1]_i_6_n_0 ),
        .O(\FSM_sequential_q[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h6FF6)) 
    \FSM_sequential_q[0]_i_9 
       (.I0(dip[2]),
        .I1(\q_reg[2]__0_0 ),
        .I2(dip[3]),
        .I3(\q_reg[3]__0_0 ),
        .O(\FSM_sequential_q[0]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h0044AAAE)) 
    \FSM_sequential_q[1]_i_1 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\FSM_sequential_q[1]_i_2_n_0 ),
        .I3(\FSM_sequential_q[1]_i_3_n_0 ),
        .I4(\FSM_sequential_q[1]_i_4_n_0 ),
        .O(\FSM_sequential_q[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \FSM_sequential_q[1]_i_10 
       (.I0(\FSM_sequential_q[1]_i_14_n_0 ),
        .I1(\FSM_sequential_q[1]_i_15_n_0 ),
        .I2(\FSM_sequential_q[0]_i_6_n_0 ),
        .I3(\FSM_sequential_q[1]_i_16_n_0 ),
        .I4(\FSM_sequential_q[0]_i_5_n_0 ),
        .I5(\FSM_sequential_q[1]_i_17_n_0 ),
        .O(\FSM_sequential_q[1]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h00000004)) 
    \FSM_sequential_q[1]_i_11 
       (.I0(\q[3]__0_i_18_n_0 ),
        .I1(\q[3]__0_i_17_n_0 ),
        .I2(\q[3]__0_i_10_n_0 ),
        .I3(\FSM_sequential_q[1]_i_18_n_0 ),
        .I4(\q[3]__0_i_15_n_0 ),
        .O(\FSM_sequential_q[1]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    \FSM_sequential_q[1]_i_12 
       (.I0(\q_reg_n_0_[11] ),
        .I1(\q_reg_n_0_[10] ),
        .I2(\q_reg_n_0_[27] ),
        .I3(\q_reg_n_0_[14] ),
        .O(\FSM_sequential_q[1]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005444)) 
    \FSM_sequential_q[1]_i_13 
       (.I0(\FSM_sequential_q[1]_i_19_n_0 ),
        .I1(\q_reg_n_0_[14] ),
        .I2(\q_reg_n_0_[27] ),
        .I3(\q_reg_n_0_[26] ),
        .I4(\FSM_sequential_q[1]_i_20_n_0 ),
        .I5(\q[31]_i_9_n_0 ),
        .O(\FSM_sequential_q[1]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAACCFFF0AACC00F0)) 
    \FSM_sequential_q[1]_i_14 
       (.I0(p_1_out[9]),
        .I1(p_3_out[9]),
        .I2(p_7_out[9]),
        .I3(\q_reg_n_0_[0] ),
        .I4(\q_reg_n_0_[1] ),
        .I5(p_5_out[9]),
        .O(\FSM_sequential_q[1]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hCCAAFFF0CCAA00F0)) 
    \FSM_sequential_q[1]_i_15 
       (.I0(\pb_shift_reg_n_0_[2][10] ),
        .I1(\pb_shift_reg_n_0_[3][10] ),
        .I2(\pb_shift_reg_n_0_[0][10] ),
        .I3(\q_reg_n_0_[0] ),
        .I4(\q_reg_n_0_[1] ),
        .I5(\pb_shift_reg_n_0_[1][10] ),
        .O(\FSM_sequential_q[1]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \FSM_sequential_q[1]_i_16 
       (.I0(p_7_out[3]),
        .I1(p_5_out[3]),
        .I2(p_3_out[3]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .I5(p_1_out[3]),
        .O(\FSM_sequential_q[1]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hAAFFF0CCAA00F0CC)) 
    \FSM_sequential_q[1]_i_17 
       (.I0(p_1_out[8]),
        .I1(p_7_out[8]),
        .I2(p_3_out[8]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .I5(p_5_out[8]),
        .O(\FSM_sequential_q[1]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFBFFFFFFF)) 
    \FSM_sequential_q[1]_i_18 
       (.I0(\q_reg_n_0_[17] ),
        .I1(\q_reg_n_0_[16] ),
        .I2(\q_reg_n_0_[13] ),
        .I3(\q_reg_n_0_[14] ),
        .I4(\q[31]_i_19_n_0 ),
        .I5(\q[3]__0_i_32_n_0 ),
        .O(\FSM_sequential_q[1]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFDFFFFF)) 
    \FSM_sequential_q[1]_i_19 
       (.I0(\q_reg_n_0_[7] ),
        .I1(\q_reg_n_0_[8] ),
        .I2(\q_reg_n_0_[9] ),
        .I3(\q_reg_n_0_[19] ),
        .I4(\q_reg_n_0_[18] ),
        .I5(\q_reg_n_0_[16] ),
        .O(\FSM_sequential_q[1]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \FSM_sequential_q[1]_i_2 
       (.I0(\FSM_sequential_q[1]_i_5_n_0 ),
        .I1(\FSM_sequential_q[1]_i_6_n_0 ),
        .I2(\FSM_sequential_q[1]_i_7_n_0 ),
        .I3(\FSM_sequential_q[1]_i_8_n_0 ),
        .I4(\FSM_sequential_q[1]_i_9_n_0 ),
        .I5(\FSM_sequential_q[1]_i_10_n_0 ),
        .O(\FSM_sequential_q[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFBAFF)) 
    \FSM_sequential_q[1]_i_20 
       (.I0(\FSM_sequential_q[1]_i_21_n_0 ),
        .I1(\q_reg_n_0_[10] ),
        .I2(\q_reg_n_0_[6] ),
        .I3(\q_reg_n_0_[15] ),
        .I4(\q_reg_n_0_[11] ),
        .I5(\FSM_sequential_q[1]_i_22_n_0 ),
        .O(\FSM_sequential_q[1]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFFFFFFFFFF)) 
    \FSM_sequential_q[1]_i_21 
       (.I0(\q_reg_n_0_[20] ),
        .I1(\q_reg_n_0_[25] ),
        .I2(\q_reg_n_0_[24] ),
        .I3(\q_reg_n_0_[17] ),
        .I4(\q_reg_n_0_[12] ),
        .I5(\q_reg_n_0_[13] ),
        .O(\FSM_sequential_q[1]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \FSM_sequential_q[1]_i_22 
       (.I0(\q_reg_n_0_[21] ),
        .I1(\q_reg_n_0_[22] ),
        .I2(\q_reg_n_0_[23] ),
        .O(\FSM_sequential_q[1]_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_sequential_q[1]_i_3 
       (.I0(\q_reg_n_0_[1] ),
        .I1(\q_reg_n_0_[0] ),
        .O(\FSM_sequential_q[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hA000A030A000A000)) 
    \FSM_sequential_q[1]_i_4 
       (.I0(\FSM_sequential_q[1]_i_11_n_0 ),
        .I1(\q[3]__0_i_10_n_0 ),
        .I2(q_reg[1]),
        .I3(q_reg[0]),
        .I4(\FSM_sequential_q[1]_i_12_n_0 ),
        .I5(\FSM_sequential_q[1]_i_13_n_0 ),
        .O(\FSM_sequential_q[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF0AACCFFF0AACC00)) 
    \FSM_sequential_q[1]_i_5 
       (.I0(p_3_out[6]),
        .I1(p_5_out[6]),
        .I2(p_1_out[6]),
        .I3(\q_reg_n_0_[0] ),
        .I4(\q_reg_n_0_[1] ),
        .I5(p_7_out[6]),
        .O(\FSM_sequential_q[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF0AACCFFF0AACC00)) 
    \FSM_sequential_q[1]_i_6 
       (.I0(p_3_out[7]),
        .I1(p_5_out[7]),
        .I2(p_1_out[7]),
        .I3(\q_reg_n_0_[0] ),
        .I4(\q_reg_n_0_[1] ),
        .I5(p_7_out[7]),
        .O(\FSM_sequential_q[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \FSM_sequential_q[1]_i_7 
       (.I0(p_7_out[4]),
        .I1(p_5_out[4]),
        .I2(p_3_out[4]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .I5(p_1_out[4]),
        .O(\FSM_sequential_q[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAAFFF0CCAA00F0CC)) 
    \FSM_sequential_q[1]_i_8 
       (.I0(p_1_out[2]),
        .I1(p_7_out[2]),
        .I2(p_3_out[2]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .I5(p_5_out[2]),
        .O(\FSM_sequential_q[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hCCAAFFF0CCAA00F0)) 
    \FSM_sequential_q[1]_i_9 
       (.I0(p_3_out[10]),
        .I1(p_1_out[10]),
        .I2(p_7_out[10]),
        .I3(\q_reg_n_0_[0] ),
        .I4(\q_reg_n_0_[1] ),
        .I5(p_5_out[10]),
        .O(\FSM_sequential_q[1]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "HANDLE_LONG_PRESS:01,WAIT_FOR_INPUT:00,LOSE:11,WIN:10" *) 
  FDCE \FSM_sequential_q_reg[0] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\FSM_sequential_q[0]_i_1_n_0 ),
        .Q(q_reg[0]));
  (* FSM_ENCODED_STATES = "HANDLE_LONG_PRESS:01,WAIT_FOR_INPUT:00,LOSE:11,WIN:10" *) 
  FDCE \FSM_sequential_q_reg[1] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\FSM_sequential_q[1]_i_1_n_0 ),
        .Q(q_reg[1]));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry
       (.CI(1'b0),
        .CO({d0_carry_n_0,d0_carry_n_1,d0_carry_n_2,d0_carry_n_3}),
        .CYINIT(\q_reg[0]__0_n_0 ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry_n_4,d0_carry_n_5,d0_carry_n_6,d0_carry_n_7}),
        .S({\q_reg_n_0_[4] ,\q_reg_n_0_[3] ,\q_reg_n_0_[2] ,\q_reg[1]__0_n_0 }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__0
       (.CI(d0_carry_n_0),
        .CO({d0_carry__0_n_0,d0_carry__0_n_1,d0_carry__0_n_2,d0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry__0_n_4,d0_carry__0_n_5,d0_carry__0_n_6,d0_carry__0_n_7}),
        .S({\q_reg_n_0_[8] ,\q_reg_n_0_[7] ,\q_reg_n_0_[6] ,\q_reg_n_0_[5] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__1
       (.CI(d0_carry__0_n_0),
        .CO({d0_carry__1_n_0,d0_carry__1_n_1,d0_carry__1_n_2,d0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry__1_n_4,d0_carry__1_n_5,d0_carry__1_n_6,d0_carry__1_n_7}),
        .S({\q_reg_n_0_[12] ,\q_reg_n_0_[11] ,\q_reg_n_0_[10] ,\q_reg_n_0_[9] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__2
       (.CI(d0_carry__1_n_0),
        .CO({d0_carry__2_n_0,d0_carry__2_n_1,d0_carry__2_n_2,d0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry__2_n_4,d0_carry__2_n_5,d0_carry__2_n_6,d0_carry__2_n_7}),
        .S({\q_reg_n_0_[16] ,\q_reg_n_0_[15] ,\q_reg_n_0_[14] ,\q_reg_n_0_[13] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__3
       (.CI(d0_carry__2_n_0),
        .CO({d0_carry__3_n_0,d0_carry__3_n_1,d0_carry__3_n_2,d0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry__3_n_4,d0_carry__3_n_5,d0_carry__3_n_6,d0_carry__3_n_7}),
        .S({\q_reg_n_0_[20] ,\q_reg_n_0_[19] ,\q_reg_n_0_[18] ,\q_reg_n_0_[17] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__4
       (.CI(d0_carry__3_n_0),
        .CO({d0_carry__4_n_0,d0_carry__4_n_1,d0_carry__4_n_2,d0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry__4_n_4,d0_carry__4_n_5,d0_carry__4_n_6,d0_carry__4_n_7}),
        .S({\q_reg_n_0_[24] ,\q_reg_n_0_[23] ,\q_reg_n_0_[22] ,\q_reg_n_0_[21] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__5
       (.CI(d0_carry__4_n_0),
        .CO({d0_carry__5_n_0,d0_carry__5_n_1,d0_carry__5_n_2,d0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({d0_carry__5_n_4,d0_carry__5_n_5,d0_carry__5_n_6,d0_carry__5_n_7}),
        .S({p_0_in0,\q_reg_n_0_[27] ,\q_reg_n_0_[26] ,\q_reg_n_0_[25] }));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 d0_carry__6
       (.CI(d0_carry__5_n_0),
        .CO({NLW_d0_carry__6_CO_UNCONNECTED[3:2],d0_carry__6_n_2,d0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_d0_carry__6_O_UNCONNECTED[3],d0_carry__6_n_5,d0_carry__6_n_6,d0_carry__6_n_7}),
        .S({1'b0,\q_reg_n_0_[31] ,\q_reg_n_0_[30] ,\q_reg_n_0_[29] }));
  FDCE \pb_shift_reg[0][0] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(pb[0]),
        .Q(p_7_out[1]));
  FDCE \pb_shift_reg[0][10] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[10]),
        .Q(\pb_shift_reg_n_0_[0][10] ));
  FDCE \pb_shift_reg[0][1] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[1]),
        .Q(p_7_out[2]));
  FDCE \pb_shift_reg[0][2] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[2]),
        .Q(p_7_out[3]));
  FDCE \pb_shift_reg[0][3] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[3]),
        .Q(p_7_out[4]));
  FDCE \pb_shift_reg[0][4] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[4]),
        .Q(p_7_out[5]));
  FDCE \pb_shift_reg[0][5] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[5]),
        .Q(p_7_out[6]));
  FDCE \pb_shift_reg[0][6] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[6]),
        .Q(p_7_out[7]));
  FDCE \pb_shift_reg[0][7] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[7]),
        .Q(p_7_out[8]));
  FDCE \pb_shift_reg[0][8] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[8]),
        .Q(p_7_out[9]));
  FDCE \pb_shift_reg[0][9] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_7_out[9]),
        .Q(p_7_out[10]));
  FDCE \pb_shift_reg[1][0] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(pb[1]),
        .Q(p_5_out[1]));
  FDCE \pb_shift_reg[1][10] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[10]),
        .Q(\pb_shift_reg_n_0_[1][10] ));
  FDCE \pb_shift_reg[1][1] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[1]),
        .Q(p_5_out[2]));
  FDCE \pb_shift_reg[1][2] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[2]),
        .Q(p_5_out[3]));
  FDCE \pb_shift_reg[1][3] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[3]),
        .Q(p_5_out[4]));
  FDCE \pb_shift_reg[1][4] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[4]),
        .Q(p_5_out[5]));
  FDCE \pb_shift_reg[1][5] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[5]),
        .Q(p_5_out[6]));
  FDCE \pb_shift_reg[1][6] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[6]),
        .Q(p_5_out[7]));
  FDCE \pb_shift_reg[1][7] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[7]),
        .Q(p_5_out[8]));
  FDCE \pb_shift_reg[1][8] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[8]),
        .Q(p_5_out[9]));
  FDCE \pb_shift_reg[1][9] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_5_out[9]),
        .Q(p_5_out[10]));
  FDCE \pb_shift_reg[2][0] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(pb[2]),
        .Q(p_3_out[1]));
  FDCE \pb_shift_reg[2][10] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[10]),
        .Q(\pb_shift_reg_n_0_[2][10] ));
  FDCE \pb_shift_reg[2][1] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[1]),
        .Q(p_3_out[2]));
  FDCE \pb_shift_reg[2][2] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[2]),
        .Q(p_3_out[3]));
  FDCE \pb_shift_reg[2][3] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[3]),
        .Q(p_3_out[4]));
  FDCE \pb_shift_reg[2][4] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[4]),
        .Q(p_3_out[5]));
  FDCE \pb_shift_reg[2][5] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[5]),
        .Q(p_3_out[6]));
  FDCE \pb_shift_reg[2][6] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[6]),
        .Q(p_3_out[7]));
  FDCE \pb_shift_reg[2][7] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[7]),
        .Q(p_3_out[8]));
  FDCE \pb_shift_reg[2][8] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[8]),
        .Q(p_3_out[9]));
  FDCE \pb_shift_reg[2][9] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_3_out[9]),
        .Q(p_3_out[10]));
  FDCE \pb_shift_reg[3][0] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(pb[3]),
        .Q(p_1_out[1]));
  FDCE \pb_shift_reg[3][10] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[10]),
        .Q(\pb_shift_reg_n_0_[3][10] ));
  FDCE \pb_shift_reg[3][1] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[1]),
        .Q(p_1_out[2]));
  FDCE \pb_shift_reg[3][2] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[2]),
        .Q(p_1_out[3]));
  FDCE \pb_shift_reg[3][3] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[3]),
        .Q(p_1_out[4]));
  FDCE \pb_shift_reg[3][4] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[4]),
        .Q(p_1_out[5]));
  FDCE \pb_shift_reg[3][5] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[5]),
        .Q(p_1_out[6]));
  FDCE \pb_shift_reg[3][6] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[6]),
        .Q(p_1_out[7]));
  FDCE \pb_shift_reg[3][7] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[7]),
        .Q(p_1_out[8]));
  FDCE \pb_shift_reg[3][8] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[8]),
        .Q(p_1_out[9]));
  FDCE \pb_shift_reg[3][9] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_1_out[9]),
        .Q(p_1_out[10]));
  LUT6 #(
    .INIT(64'h5555100055555555)) 
    \q[0]__0_i_1 
       (.I0(\q_reg[0]__0_n_0 ),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[0]__0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBABABAFFBABABA00)) 
    \q[0]__1_i_1 
       (.I0(\q[1]__1_i_3_n_0 ),
        .I1(\q[3]__0_i_2_n_0 ),
        .I2(\q[0]__1_i_2_n_0 ),
        .I3(\q[0]__1_i_3_n_0 ),
        .I4(\q[0]__1_i_4_n_0 ),
        .I5(\q_reg[0]__1_0 ),
        .O(\q[0]__1_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFE0FFE0E0E0E0)) 
    \q[0]__1_i_10 
       (.I0(\q_reg_n_0_[26] ),
        .I1(\q_reg_n_0_[27] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q_reg_n_0_[9] ),
        .I4(\q_reg_n_0_[14] ),
        .I5(\q_reg_n_0_[18] ),
        .O(\q[0]__1_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h000000F8)) 
    \q[0]__1_i_11 
       (.I0(\q_reg_n_0_[26] ),
        .I1(\q_reg_n_0_[27] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q[2]__0_i_14_n_0 ),
        .I4(\q[0]__1_i_13_n_0 ),
        .O(\q[0]__1_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \q[0]__1_i_12 
       (.I0(\q_reg_n_0_[17] ),
        .I1(\q_reg_n_0_[23] ),
        .I2(q_reg[1]),
        .I3(\q_reg_n_0_[5] ),
        .O(\q[0]__1_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \q[0]__1_i_13 
       (.I0(\q_reg_n_0_[21] ),
        .I1(\q_reg_n_0_[22] ),
        .I2(\q_reg_n_0_[11] ),
        .I3(\q_reg_n_0_[10] ),
        .O(\q[0]__1_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \q[0]__1_i_2 
       (.I0(\q_reg_n_0_[0] ),
        .I1(\q_reg_n_0_[1] ),
        .I2(\FSM_sequential_q[0]_i_3_n_0 ),
        .O(\q[0]__1_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100110011)) 
    \q[0]__1_i_3 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\q[3]__0_i_10_n_0 ),
        .I3(\q[0]__1_i_5_n_0 ),
        .I4(\q[3]__0_i_12_n_0 ),
        .I5(\FSM_sequential_q[0]_i_3_n_0 ),
        .O(\q[0]__1_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFD50000)) 
    \q[0]__1_i_4 
       (.I0(\q[3]__0_i_8_n_0 ),
        .I1(\q[0]__1_i_6_n_0 ),
        .I2(\q[3]__0_i_11_n_0 ),
        .I3(q_reg[1]),
        .I4(q_reg[0]),
        .I5(\q[0]__1_i_7_n_0 ),
        .O(\q[0]__1_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \q[0]__1_i_5 
       (.I0(\q_reg_n_0_[1] ),
        .I1(\q_reg_n_0_[0] ),
        .O(\q[0]__1_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \q[0]__1_i_6 
       (.I0(\q_reg_n_0_[0] ),
        .I1(\q_reg_n_0_[1] ),
        .I2(\q[3]__0_i_10_n_0 ),
        .O(\q[0]__1_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h04000000)) 
    \q[0]__1_i_7 
       (.I0(\q[0]__1_i_8_n_0 ),
        .I1(\q[0]__1_i_9_n_0 ),
        .I2(\q[0]__1_i_10_n_0 ),
        .I3(\q[0]__1_i_11_n_0 ),
        .I4(\q[31]_i_12_n_0 ),
        .O(\q[0]__1_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFDB)) 
    \q[0]__1_i_8 
       (.I0(\q_reg_n_0_[19] ),
        .I1(\q_reg_n_0_[18] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q[0]__1_i_12_n_0 ),
        .I4(\q[31]_i_17_n_0 ),
        .I5(\q[31]_i_22_n_0 ),
        .O(\q[0]__1_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000F20000)) 
    \q[0]__1_i_9 
       (.I0(\q_reg_n_0_[14] ),
        .I1(\q_reg_n_0_[9] ),
        .I2(\q_reg_n_0_[18] ),
        .I3(\q_reg_n_0_[24] ),
        .I4(\q_reg_n_0_[25] ),
        .I5(\q_reg_n_0_[8] ),
        .O(\q[0]__1_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFB04)) 
    \q[0]_i_1 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\FSM_sequential_q[1]_i_2_n_0 ),
        .I3(\q_reg_n_0_[0] ),
        .O(\q[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \q[0]_i_2 
       (.I0(\q_reg[0]__2_n_0 ),
        .O(\q[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[10]_i_1 
       (.I0(d0_carry__1_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[11]_i_1 
       (.I0(d0_carry__1_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[12]_i_1 
       (.I0(d0_carry__1_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[13]_i_1 
       (.I0(d0_carry__2_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[14]_i_1 
       (.I0(d0_carry__2_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[15]_i_1 
       (.I0(d0_carry__2_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[16]_i_1 
       (.I0(d0_carry__2_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[17]_i_1 
       (.I0(d0_carry__3_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[18]_i_1 
       (.I0(d0_carry__3_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[19]_i_1 
       (.I0(d0_carry__3_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[1]__0_i_1 
       (.I0(d0_carry_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[1]__0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF4F4F4FFF4F4F400)) 
    \q[1]__1_i_1 
       (.I0(\q[3]__0_i_2_n_0 ),
        .I1(\q[1]__1_i_2_n_0 ),
        .I2(\q[1]__1_i_3_n_0 ),
        .I3(\q[1]__1_i_4_n_0 ),
        .I4(\q[1]__1_i_5_n_0 ),
        .I5(\q_reg[1]__1_0 ),
        .O(\q[1]__1_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hFEBF)) 
    \q[1]__1_i_10 
       (.I0(\q[1]__1_i_12_n_0 ),
        .I1(\q_reg_n_0_[24] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q_reg_n_0_[17] ),
        .O(\q[1]__1_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFBDFFFFFFFFBDFF)) 
    \q[1]__1_i_11 
       (.I0(\q_reg_n_0_[24] ),
        .I1(\q_reg_n_0_[25] ),
        .I2(\q_reg_n_0_[27] ),
        .I3(\q_reg_n_0_[17] ),
        .I4(\q_reg_n_0_[16] ),
        .I5(\q_reg_n_0_[14] ),
        .O(\q[1]__1_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFFFFDF)) 
    \q[1]__1_i_12 
       (.I0(\q_reg_n_0_[15] ),
        .I1(\q_reg_n_0_[6] ),
        .I2(q_reg[1]),
        .I3(\q_reg_n_0_[10] ),
        .I4(\q_reg_n_0_[11] ),
        .O(\q[1]__1_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \q[1]__1_i_2 
       (.I0(\q_reg_n_0_[0] ),
        .I1(\q_reg_n_0_[1] ),
        .I2(\FSM_sequential_q[0]_i_3_n_0 ),
        .O(\q[1]__1_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00400F40)) 
    \q[1]__1_i_3 
       (.I0(\q[3]__0_i_7_n_0 ),
        .I1(\q[3]__0_i_8_n_0 ),
        .I2(q_reg[0]),
        .I3(q_reg[1]),
        .I4(\q_reg_n_0_[27] ),
        .I5(\q[3]__0_i_9_n_0 ),
        .O(\q[1]__1_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h888A8888AAAAAAAA)) 
    \q[1]__1_i_4 
       (.I0(q_reg[0]),
        .I1(q_reg[1]),
        .I2(\q[1]__1_i_6_n_0 ),
        .I3(\q[3]__0_i_10_n_0 ),
        .I4(\q[3]__0_i_11_n_0 ),
        .I5(\q[3]__0_i_8_n_0 ),
        .O(\q[1]__1_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF11130000)) 
    \q[1]__1_i_5 
       (.I0(\FSM_sequential_q[0]_i_3_n_0 ),
        .I1(\q[1]__1_i_6_n_0 ),
        .I2(\q[3]__0_i_10_n_0 ),
        .I3(\q[3]__0_i_12_n_0 ),
        .I4(\q[31]_i_3_n_0 ),
        .I5(\q[1]__1_i_7_n_0 ),
        .O(\q[1]__1_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[1]__1_i_6 
       (.I0(\q_reg_n_0_[1] ),
        .I1(\q_reg_n_0_[0] ),
        .O(\q[1]__1_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h00000002)) 
    \q[1]__1_i_7 
       (.I0(\q[1]__1_i_8_n_0 ),
        .I1(\q[1]__1_i_9_n_0 ),
        .I2(\q[3]__0_i_29_n_0 ),
        .I3(\q[1]__1_i_10_n_0 ),
        .I4(\q[1]__1_i_11_n_0 ),
        .O(\q[1]__1_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000010000400000)) 
    \q[1]__1_i_8 
       (.I0(\q[31]_i_9_n_0 ),
        .I1(\q_reg_n_0_[7] ),
        .I2(\q_reg_n_0_[9] ),
        .I3(\q_reg_n_0_[8] ),
        .I4(\q_reg_n_0_[12] ),
        .I5(\q_reg_n_0_[14] ),
        .O(\q[1]__1_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFDFFF)) 
    \q[1]__1_i_9 
       (.I0(\q_reg_n_0_[26] ),
        .I1(\q_reg_n_0_[19] ),
        .I2(\q_reg_n_0_[13] ),
        .I3(\q_reg_n_0_[18] ),
        .I4(\q_reg[0]__0_n_0 ),
        .I5(\q_reg_n_0_[3] ),
        .O(\q[1]__1_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFBFF0400)) 
    \q[1]_i_1 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\FSM_sequential_q[1]_i_2_n_0 ),
        .I3(\q_reg_n_0_[0] ),
        .I4(\q_reg_n_0_[1] ),
        .O(\q[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[20]_i_1 
       (.I0(d0_carry__3_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[21]_i_1 
       (.I0(d0_carry__4_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[22]_i_1 
       (.I0(d0_carry__4_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[23]_i_1 
       (.I0(d0_carry__4_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[24]_i_1 
       (.I0(d0_carry__4_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[25]_i_1 
       (.I0(d0_carry__5_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[26]_i_1 
       (.I0(d0_carry__5_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[27]_i_1 
       (.I0(d0_carry__5_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[28]_i_1 
       (.I0(d0_carry__5_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[29]_i_1 
       (.I0(d0_carry__6_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBABABAFFBABABA00)) 
    \q[2]__0_i_1 
       (.I0(\q[3]__0_i_4_n_0 ),
        .I1(\q[3]__0_i_2_n_0 ),
        .I2(\q[2]__0_i_2_n_0 ),
        .I3(\q[2]__0_i_3_n_0 ),
        .I4(\q[2]__0_i_4_n_0 ),
        .I5(\q_reg[2]__0_0 ),
        .O(\q[2]__0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1011000000000000)) 
    \q[2]__0_i_10 
       (.I0(\q_reg_n_0_[6] ),
        .I1(\q_reg_n_0_[11] ),
        .I2(\q_reg_n_0_[26] ),
        .I3(\q_reg_n_0_[25] ),
        .I4(\q_reg_n_0_[15] ),
        .I5(\q_reg_n_0_[12] ),
        .O(\q[2]__0_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[2]__0_i_11 
       (.I0(\q_reg_n_0_[5] ),
        .I1(q_reg[1]),
        .O(\q[2]__0_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFBFFFFDF)) 
    \q[2]__0_i_12 
       (.I0(\q_reg_n_0_[14] ),
        .I1(\q_reg_n_0_[13] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q_reg_n_0_[18] ),
        .I4(\q_reg_n_0_[17] ),
        .O(\q[2]__0_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \q[2]__0_i_13 
       (.I0(\q_reg_n_0_[23] ),
        .I1(\q_reg_n_0_[24] ),
        .I2(\q_reg_n_0_[7] ),
        .I3(\q_reg_n_0_[19] ),
        .O(\q[2]__0_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[2]__0_i_14 
       (.I0(\q_reg[1]__0_n_0 ),
        .I1(\q_reg[0]__0_n_0 ),
        .I2(\q_reg_n_0_[16] ),
        .I3(\q_reg_n_0_[4] ),
        .O(\q[2]__0_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \q[2]__0_i_2 
       (.I0(\q_reg_n_0_[1] ),
        .I1(\q_reg_n_0_[0] ),
        .I2(\FSM_sequential_q[0]_i_3_n_0 ),
        .O(\q[2]__0_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100001111)) 
    \q[2]__0_i_3 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\q[3]__0_i_12_n_0 ),
        .I3(\q[3]__0_i_10_n_0 ),
        .I4(\q[2]__0_i_5_n_0 ),
        .I5(\FSM_sequential_q[0]_i_3_n_0 ),
        .O(\q[2]__0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF5D0000)) 
    \q[2]__0_i_4 
       (.I0(\q[3]__0_i_8_n_0 ),
        .I1(\q[3]__0_i_11_n_0 ),
        .I2(\q[2]__0_i_6_n_0 ),
        .I3(q_reg[1]),
        .I4(q_reg[0]),
        .I5(\q[2]__0_i_7_n_0 ),
        .O(\q[2]__0_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[2]__0_i_5 
       (.I0(\q_reg_n_0_[0] ),
        .I1(\q_reg_n_0_[1] ),
        .O(\q[2]__0_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \q[2]__0_i_6 
       (.I0(\q_reg_n_0_[1] ),
        .I1(\q_reg_n_0_[0] ),
        .I2(\q[3]__0_i_10_n_0 ),
        .O(\q[2]__0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010040000)) 
    \q[2]__0_i_7 
       (.I0(\q[31]_i_9_n_0 ),
        .I1(\q_reg_n_0_[8] ),
        .I2(\q_reg_n_0_[9] ),
        .I3(\q_reg_n_0_[13] ),
        .I4(\q[2]__0_i_8_n_0 ),
        .I5(\q[2]__0_i_9_n_0 ),
        .O(\q[2]__0_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000002A00000020)) 
    \q[2]__0_i_8 
       (.I0(\q[2]__0_i_10_n_0 ),
        .I1(\q_reg_n_0_[26] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q[2]__0_i_11_n_0 ),
        .I4(\q[31]_i_19_n_0 ),
        .I5(\q_reg_n_0_[25] ),
        .O(\q[2]__0_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    \q[2]__0_i_9 
       (.I0(\q[2]__0_i_12_n_0 ),
        .I1(\q[2]__0_i_13_n_0 ),
        .I2(\q_reg_n_0_[10] ),
        .I3(\q_reg_n_0_[27] ),
        .I4(\q[31]_i_17_n_0 ),
        .I5(\q[2]__0_i_14_n_0 ),
        .O(\q[2]__0_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[2]_i_1 
       (.I0(d0_carry_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[30]_i_1 
       (.I0(d0_carry__6_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[31]_i_1 
       (.I0(d0_carry__6_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFFFFFF)) 
    \q[31]_i_10 
       (.I0(\q_reg_n_0_[13] ),
        .I1(\q_reg_n_0_[10] ),
        .I2(\q_reg_n_0_[25] ),
        .I3(\q_reg_n_0_[26] ),
        .I4(\q_reg_n_0_[7] ),
        .I5(\q_reg_n_0_[6] ),
        .O(\q[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \q[31]_i_11 
       (.I0(\q_reg[0]__0_n_0 ),
        .I1(\q_reg[1]__0_n_0 ),
        .I2(\q[31]_i_17_n_0 ),
        .I3(\q_reg_n_0_[8] ),
        .I4(\q_reg_n_0_[9] ),
        .I5(\q[31]_i_18_n_0 ),
        .O(\q[31]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \q[31]_i_12 
       (.I0(\q_reg_n_0_[7] ),
        .I1(\q_reg_n_0_[13] ),
        .I2(\q_reg_n_0_[12] ),
        .I3(\q[31]_i_9_n_0 ),
        .O(\q[31]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    \q[31]_i_13 
       (.I0(\q_reg_n_0_[10] ),
        .I1(\q_reg_n_0_[11] ),
        .I2(\q[31]_i_19_n_0 ),
        .I3(\q_reg_n_0_[23] ),
        .I4(\q_reg_n_0_[26] ),
        .I5(\q_reg_n_0_[25] ),
        .O(\q[31]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \q[31]_i_14 
       (.I0(\q[31]_i_20_n_0 ),
        .I1(\q[31]_i_21_n_0 ),
        .I2(\q_reg_n_0_[24] ),
        .I3(\q_reg_n_0_[27] ),
        .I4(\q[31]_i_22_n_0 ),
        .I5(\q_reg_n_0_[14] ),
        .O(\q[31]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \q[31]_i_15 
       (.I0(q_reg[0]),
        .I1(q_reg[1]),
        .O(\q[31]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \q[31]_i_16 
       (.I0(\q_reg_n_0_[27] ),
        .I1(\q_reg_n_0_[24] ),
        .O(\q[31]_i_16_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \q[31]_i_17 
       (.I0(\q_reg_n_0_[2] ),
        .I1(\q_reg_n_0_[3] ),
        .O(\q[31]_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \q[31]_i_18 
       (.I0(\q_reg_n_0_[4] ),
        .I1(\q_reg_n_0_[5] ),
        .O(\q[31]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \q[31]_i_19 
       (.I0(\q_reg_n_0_[22] ),
        .I1(\q_reg_n_0_[21] ),
        .O(\q[31]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \q[31]_i_2 
       (.I0(\q[31]_i_6_n_0 ),
        .I1(\q[31]_i_7_n_0 ),
        .I2(\q[31]_i_8_n_0 ),
        .I3(\q[31]_i_9_n_0 ),
        .I4(\q[31]_i_10_n_0 ),
        .I5(\q[31]_i_11_n_0 ),
        .O(\q[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hDF)) 
    \q[31]_i_20 
       (.I0(\q_reg_n_0_[19] ),
        .I1(\q_reg_n_0_[18] ),
        .I2(\q_reg_n_0_[20] ),
        .O(\q[31]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[31]_i_21 
       (.I0(\q_reg_n_0_[16] ),
        .I1(\q_reg_n_0_[17] ),
        .O(\q[31]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[31]_i_22 
       (.I0(\q_reg_n_0_[6] ),
        .I1(\q_reg_n_0_[15] ),
        .O(\q[31]_i_22_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \q[31]_i_3 
       (.I0(q_reg[0]),
        .I1(q_reg[1]),
        .O(\q[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAAFFFBAAAA)) 
    \q[31]_i_4 
       (.I0(\q[3]__0_i_9_n_0 ),
        .I1(\FSM_sequential_q[1]_i_13_n_0 ),
        .I2(\FSM_sequential_q[1]_i_12_n_0 ),
        .I3(\q[3]__0_i_10_n_0 ),
        .I4(q_reg[1]),
        .I5(q_reg[0]),
        .O(\q[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h5555555DFFFFFFFF)) 
    \q[31]_i_5 
       (.I0(\FSM_sequential_q[1]_i_2_n_0 ),
        .I1(\q[31]_i_12_n_0 ),
        .I2(\q[31]_i_13_n_0 ),
        .I3(\q[31]_i_11_n_0 ),
        .I4(\q[31]_i_14_n_0 ),
        .I5(\q[31]_i_15_n_0 ),
        .O(\q[31]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \q[31]_i_6 
       (.I0(\q_reg_n_0_[14] ),
        .I1(\q_reg_n_0_[11] ),
        .I2(\q_reg_n_0_[17] ),
        .I3(\q_reg_n_0_[23] ),
        .O(\q[31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    \q[31]_i_7 
       (.I0(\q_reg_n_0_[19] ),
        .I1(\q_reg_n_0_[18] ),
        .I2(\q_reg_n_0_[20] ),
        .O(\q[31]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0040000000000000)) 
    \q[31]_i_8 
       (.I0(\q_reg_n_0_[22] ),
        .I1(\q_reg_n_0_[21] ),
        .I2(\q_reg_n_0_[16] ),
        .I3(\q[31]_i_16_n_0 ),
        .I4(\q_reg_n_0_[12] ),
        .I5(\q_reg_n_0_[15] ),
        .O(\q[31]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[31]_i_9 
       (.I0(p_0_in0),
        .I1(\q_reg_n_0_[29] ),
        .I2(\q_reg_n_0_[31] ),
        .I3(\q_reg_n_0_[30] ),
        .O(\q[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hF4F4F4FFF4F4F400)) 
    \q[3]__0_i_1 
       (.I0(\q[3]__0_i_2_n_0 ),
        .I1(\q[3]__0_i_3_n_0 ),
        .I2(\q[3]__0_i_4_n_0 ),
        .I3(\q[3]__0_i_5_n_0 ),
        .I4(\q[3]__0_i_6_n_0 ),
        .I5(\q_reg[3]__0_0 ),
        .O(\q[3]__0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \q[3]__0_i_10 
       (.I0(\q_reg[1]__0_n_0 ),
        .I1(\q_reg_n_0_[4] ),
        .I2(\q_reg_n_0_[5] ),
        .I3(\q_reg_n_0_[2] ),
        .I4(\q_reg_n_0_[3] ),
        .I5(\q_reg[0]__0_n_0 ),
        .O(\q[3]__0_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \q[3]__0_i_11 
       (.I0(\q[3]__0_i_19_n_0 ),
        .I1(\q[3]__0_i_20_n_0 ),
        .I2(\q[31]_i_9_n_0 ),
        .I3(\q[31]_i_13_n_0 ),
        .I4(\q[3]__0_i_21_n_0 ),
        .I5(\q[3]__0_i_22_n_0 ),
        .O(\q[3]__0_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[3]__0_i_12 
       (.I0(\q[3]__0_i_23_n_0 ),
        .I1(\q[3]__0_i_24_n_0 ),
        .I2(\q[3]__0_i_25_n_0 ),
        .I3(\q[3]__0_i_26_n_0 ),
        .O(\q[3]__0_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000002000020)) 
    \q[3]__0_i_13 
       (.I0(\q[3]__0_i_27_n_0 ),
        .I1(\q[3]__0_i_28_n_0 ),
        .I2(\q_reg_n_0_[13] ),
        .I3(\q_reg_n_0_[14] ),
        .I4(\q_reg_n_0_[16] ),
        .I5(\q[3]__0_i_29_n_0 ),
        .O(\q[3]__0_i_13_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \q[3]__0_i_14 
       (.I0(\FSM_sequential_q[1]_i_9_n_0 ),
        .I1(\FSM_sequential_q[1]_i_8_n_0 ),
        .I2(\FSM_sequential_q[1]_i_7_n_0 ),
        .I3(\FSM_sequential_q[1]_i_6_n_0 ),
        .I4(\FSM_sequential_q[1]_i_5_n_0 ),
        .O(\q[3]__0_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF7FFFFFFF)) 
    \q[3]__0_i_15 
       (.I0(\q_reg_n_0_[10] ),
        .I1(p_0_in0),
        .I2(\q_reg_n_0_[22] ),
        .I3(\q_reg_n_0_[23] ),
        .I4(\q[3]__0_i_30_n_0 ),
        .I5(\q[3]__0_i_31_n_0 ),
        .O(\q[3]__0_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEFFFFF)) 
    \q[3]__0_i_16 
       (.I0(\q[3]__0_i_10_n_0 ),
        .I1(\q[3]__0_i_32_n_0 ),
        .I2(\q[31]_i_19_n_0 ),
        .I3(\q[3]__0_i_33_n_0 ),
        .I4(\q_reg_n_0_[16] ),
        .I5(\q_reg_n_0_[17] ),
        .O(\q[3]__0_i_16_n_0 ));
  LUT5 #(
    .INIT(32'h00400000)) 
    \q[3]__0_i_17 
       (.I0(\q[3]__0_i_34_n_0 ),
        .I1(\q_reg_n_0_[8] ),
        .I2(\q_reg_n_0_[23] ),
        .I3(\q_reg_n_0_[15] ),
        .I4(\q_reg_n_0_[19] ),
        .O(\q[3]__0_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \q[3]__0_i_18 
       (.I0(\q[3]__0_i_35_n_0 ),
        .I1(\q_reg_n_0_[14] ),
        .I2(\q_reg_n_0_[7] ),
        .I3(\q_reg_n_0_[9] ),
        .I4(\q_reg_n_0_[6] ),
        .I5(\q_reg_n_0_[11] ),
        .O(\q[3]__0_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF70FFFFFFFFFF)) 
    \q[3]__0_i_19 
       (.I0(\q_reg_n_0_[13] ),
        .I1(\q_reg_n_0_[12] ),
        .I2(\q_reg_n_0_[14] ),
        .I3(\q_reg_n_0_[20] ),
        .I4(\q_reg_n_0_[18] ),
        .I5(\q_reg_n_0_[19] ),
        .O(\q[3]__0_i_19_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \q[3]__0_i_2 
       (.I0(\q[3]__0_i_7_n_0 ),
        .I1(q_reg[1]),
        .I2(q_reg[0]),
        .O(\q[3]__0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h07)) 
    \q[3]__0_i_20 
       (.I0(\q_reg_n_0_[6] ),
        .I1(\q_reg_n_0_[7] ),
        .I2(\q_reg_n_0_[8] ),
        .O(\q[3]__0_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF8FFFFFFFF)) 
    \q[3]__0_i_21 
       (.I0(\q_reg_n_0_[25] ),
        .I1(\q_reg_n_0_[24] ),
        .I2(\q[3]__0_i_33_n_0 ),
        .I3(\q[31]_i_21_n_0 ),
        .I4(\q_reg_n_0_[8] ),
        .I5(\q_reg_n_0_[7] ),
        .O(\q[3]__0_i_21_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \q[3]__0_i_22 
       (.I0(\q[3]__0_i_36_n_0 ),
        .I1(\q_reg_n_0_[26] ),
        .I2(\q_reg_n_0_[15] ),
        .I3(\q_reg_n_0_[11] ),
        .O(\q[3]__0_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAABF)) 
    \q[3]__0_i_23 
       (.I0(\q[31]_i_6_n_0 ),
        .I1(\q_reg_n_0_[6] ),
        .I2(\q_reg_n_0_[7] ),
        .I3(\q_reg_n_0_[8] ),
        .I4(\q[31]_i_7_n_0 ),
        .I5(\q[3]__0_i_36_n_0 ),
        .O(\q[3]__0_i_23_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFEE)) 
    \q[3]__0_i_24 
       (.I0(\q[31]_i_9_n_0 ),
        .I1(\q[3]__0_i_37_n_0 ),
        .I2(\q_reg_n_0_[12] ),
        .I3(\q_reg_n_0_[14] ),
        .I4(\q_reg_n_0_[13] ),
        .I5(\q_reg_n_0_[10] ),
        .O(\q[3]__0_i_24_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFBFFFFFFFFFF)) 
    \q[3]__0_i_25 
       (.I0(\q_reg_n_0_[29] ),
        .I1(\q_reg_n_0_[16] ),
        .I2(\q_reg_n_0_[22] ),
        .I3(\q_reg_n_0_[11] ),
        .I4(\q_reg_n_0_[24] ),
        .I5(\q[3]__0_i_33_n_0 ),
        .O(\q[3]__0_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hFF707070FF70FF70)) 
    \q[3]__0_i_26 
       (.I0(\q_reg_n_0_[16] ),
        .I1(\q_reg_n_0_[15] ),
        .I2(\q_reg_n_0_[17] ),
        .I3(\q_reg_n_0_[23] ),
        .I4(\q_reg_n_0_[22] ),
        .I5(\q_reg_n_0_[21] ),
        .O(\q[3]__0_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    \q[3]__0_i_27 
       (.I0(\q[3]__0_i_38_n_0 ),
        .I1(\q_reg_n_0_[15] ),
        .I2(\q_reg_n_0_[6] ),
        .I3(q_reg[1]),
        .I4(\q[3]__0_i_39_n_0 ),
        .I5(\q[31]_i_9_n_0 ),
        .O(\q[3]__0_i_27_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFBD)) 
    \q[3]__0_i_28 
       (.I0(\q_reg_n_0_[26] ),
        .I1(\q_reg_n_0_[19] ),
        .I2(\q_reg_n_0_[24] ),
        .I3(\q[3]__0_i_40_n_0 ),
        .I4(\q[3]__0_i_41_n_0 ),
        .I5(\q[3]__0_i_42_n_0 ),
        .O(\q[3]__0_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \q[3]__0_i_29 
       (.I0(\q_reg[1]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(\q_reg_n_0_[4] ),
        .I3(\q_reg_n_0_[5] ),
        .I4(\q_reg_n_0_[23] ),
        .I5(\q[31]_i_19_n_0 ),
        .O(\q[3]__0_i_29_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \q[3]__0_i_3 
       (.I0(\q_reg_n_0_[0] ),
        .I1(\q_reg_n_0_[1] ),
        .I2(\FSM_sequential_q[0]_i_3_n_0 ),
        .O(\q[3]__0_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \q[3]__0_i_30 
       (.I0(\q_reg_n_0_[26] ),
        .I1(\q_reg_n_0_[27] ),
        .O(\q[3]__0_i_30_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[3]__0_i_31 
       (.I0(\q_reg_n_0_[25] ),
        .I1(\q_reg_n_0_[24] ),
        .O(\q[3]__0_i_31_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[3]__0_i_32 
       (.I0(\q_reg_n_0_[20] ),
        .I1(\q_reg_n_0_[18] ),
        .O(\q[3]__0_i_32_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \q[3]__0_i_33 
       (.I0(\q_reg_n_0_[13] ),
        .I1(\q_reg_n_0_[14] ),
        .O(\q[3]__0_i_33_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \q[3]__0_i_34 
       (.I0(\q_reg_n_0_[30] ),
        .I1(\q_reg_n_0_[31] ),
        .I2(\q_reg_n_0_[29] ),
        .O(\q[3]__0_i_34_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \q[3]__0_i_35 
       (.I0(\q_reg_n_0_[13] ),
        .I1(\q_reg_n_0_[12] ),
        .O(\q[3]__0_i_35_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h4F44)) 
    \q[3]__0_i_36 
       (.I0(p_0_in0),
        .I1(\q_reg_n_0_[27] ),
        .I2(\q_reg_n_0_[10] ),
        .I3(\q_reg_n_0_[9] ),
        .O(\q[3]__0_i_36_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \q[3]__0_i_37 
       (.I0(\q_reg_n_0_[7] ),
        .I1(\q_reg_n_0_[8] ),
        .I2(\q_reg_n_0_[26] ),
        .I3(\q_reg_n_0_[25] ),
        .O(\q[3]__0_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h0081000000008100)) 
    \q[3]__0_i_38 
       (.I0(\q_reg_n_0_[12] ),
        .I1(\q_reg_n_0_[13] ),
        .I2(\q_reg_n_0_[7] ),
        .I3(\q_reg_n_0_[18] ),
        .I4(\q_reg_n_0_[19] ),
        .I5(\q_reg_n_0_[16] ),
        .O(\q[3]__0_i_38_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \q[3]__0_i_39 
       (.I0(\q_reg_n_0_[10] ),
        .I1(\q_reg_n_0_[11] ),
        .O(\q[3]__0_i_39_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F4F0F0FFF4F0)) 
    \q[3]__0_i_4 
       (.I0(\q[3]__0_i_7_n_0 ),
        .I1(\q[3]__0_i_8_n_0 ),
        .I2(\q[3]__0_i_9_n_0 ),
        .I3(q_reg[0]),
        .I4(q_reg[1]),
        .I5(\q_reg_n_0_[26] ),
        .O(\q[3]__0_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[3]__0_i_40 
       (.I0(\q_reg_n_0_[8] ),
        .I1(\q_reg_n_0_[9] ),
        .O(\q[3]__0_i_40_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \q[3]__0_i_41 
       (.I0(\q_reg_n_0_[20] ),
        .I1(\q_reg_n_0_[25] ),
        .O(\q[3]__0_i_41_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    \q[3]__0_i_42 
       (.I0(\q_reg_n_0_[27] ),
        .I1(\q_reg[0]__0_n_0 ),
        .I2(\q_reg_n_0_[17] ),
        .I3(\q_reg_n_0_[3] ),
        .O(\q[3]__0_i_42_n_0 ));
  LUT6 #(
    .INIT(64'h888A8888AAAAAAAA)) 
    \q[3]__0_i_5 
       (.I0(q_reg[0]),
        .I1(q_reg[1]),
        .I2(\FSM_sequential_q[1]_i_3_n_0 ),
        .I3(\q[3]__0_i_10_n_0 ),
        .I4(\q[3]__0_i_11_n_0 ),
        .I5(\q[3]__0_i_8_n_0 ),
        .O(\q[3]__0_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF05070000)) 
    \q[3]__0_i_6 
       (.I0(\FSM_sequential_q[0]_i_3_n_0 ),
        .I1(\q[3]__0_i_12_n_0 ),
        .I2(\FSM_sequential_q[1]_i_3_n_0 ),
        .I3(\q[3]__0_i_10_n_0 ),
        .I4(\q[31]_i_3_n_0 ),
        .I5(\q[3]__0_i_13_n_0 ),
        .O(\q[3]__0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \q[3]__0_i_7 
       (.I0(\q_reg[3]__0_0 ),
        .I1(\q_reg[2]__0_0 ),
        .I2(\q_reg_n_0_[1] ),
        .I3(\q_reg[1]__1_0 ),
        .I4(\q_reg_n_0_[0] ),
        .I5(\q_reg[0]__1_0 ),
        .O(\q[3]__0_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hFDFFFFFF)) 
    \q[3]__0_i_8 
       (.I0(\FSM_sequential_q[1]_i_10_n_0 ),
        .I1(\q[3]__0_i_14_n_0 ),
        .I2(\FSM_sequential_q[0]_i_4_n_0 ),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[0] ),
        .O(\q[3]__0_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888808888)) 
    \q[3]__0_i_9 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .I2(\q[3]__0_i_15_n_0 ),
        .I3(\q[3]__0_i_16_n_0 ),
        .I4(\q[3]__0_i_17_n_0 ),
        .I5(\q[3]__0_i_18_n_0 ),
        .O(\q[3]__0_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[3]_i_1 
       (.I0(d0_carry_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[4]_i_1 
       (.I0(d0_carry_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[5]_i_1 
       (.I0(d0_carry__0_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[6]_i_1 
       (.I0(d0_carry__0_n_6),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[7]_i_1 
       (.I0(d0_carry__0_n_5),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[8]_i_1 
       (.I0(d0_carry__0_n_4),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA2000AAAAAAAA)) 
    \q[9]_i_1 
       (.I0(d0_carry__1_n_7),
        .I1(\q[31]_i_2_n_0 ),
        .I2(\q[31]_i_3_n_0 ),
        .I3(\FSM_sequential_q[0]_i_3_n_0 ),
        .I4(\q[31]_i_4_n_0 ),
        .I5(\q[31]_i_5_n_0 ),
        .O(\q[9]_i_1_n_0 ));
  FDCE \q_reg[0] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[0]_i_1_n_0 ),
        .Q(\q_reg_n_0_[0] ));
  FDCE \q_reg[0]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[0]__0_i_1_n_0 ),
        .Q(\q_reg[0]__0_n_0 ));
  FDCE \q_reg[0]__1 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[0]__1_i_1_n_0 ),
        .Q(\q_reg[0]__1_0 ));
  FDCE \q_reg[0]__2 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1_n_7 ),
        .Q(\q_reg[0]__2_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\q_reg[0]_i_1_n_0 ,\q_reg[0]_i_1_n_1 ,\q_reg[0]_i_1_n_2 ,\q_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\q_reg[0]_i_1_n_4 ,\q_reg[0]_i_1_n_5 ,\q_reg[0]_i_1_n_6 ,\q_reg[0]_i_1_n_7 }),
        .S({\q_reg[3]__1_n_0 ,\q_reg[2]__1_n_0 ,\q_reg[1]__2_n_0 ,\q[0]_i_2_n_0 }));
  FDCE \q_reg[10] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[10]_i_1_n_0 ),
        .Q(\q_reg_n_0_[10] ));
  FDCE \q_reg[10]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1_n_5 ),
        .Q(\q_reg[10]__0_n_0 ));
  FDCE \q_reg[11] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[11]_i_1_n_0 ),
        .Q(\q_reg_n_0_[11] ));
  FDCE \q_reg[11]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1_n_4 ),
        .Q(\q_reg[11]__0_n_0 ));
  FDCE \q_reg[12] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[12]_i_1_n_0 ),
        .Q(\q_reg_n_0_[12] ));
  FDCE \q_reg[12]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1_n_7 ),
        .Q(\q_reg[12]__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[12]_i_1 
       (.CI(\q_reg[8]_i_1_n_0 ),
        .CO({\q_reg[12]_i_1_n_0 ,\q_reg[12]_i_1_n_1 ,\q_reg[12]_i_1_n_2 ,\q_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[12]_i_1_n_4 ,\q_reg[12]_i_1_n_5 ,\q_reg[12]_i_1_n_6 ,\q_reg[12]_i_1_n_7 }),
        .S({\q_reg[15]__0_n_0 ,\q_reg[14]__0_n_0 ,\q_reg[13]__0_n_0 ,\q_reg[12]__0_n_0 }));
  FDCE \q_reg[13] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[13]_i_1_n_0 ),
        .Q(\q_reg_n_0_[13] ));
  FDCE \q_reg[13]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1_n_6 ),
        .Q(\q_reg[13]__0_n_0 ));
  FDCE \q_reg[14] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[14]_i_1_n_0 ),
        .Q(\q_reg_n_0_[14] ));
  FDCE \q_reg[14]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1_n_5 ),
        .Q(\q_reg[14]__0_n_0 ));
  FDCE \q_reg[15] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[15]_i_1_n_0 ),
        .Q(\q_reg_n_0_[15] ));
  FDCE \q_reg[15]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1_n_4 ),
        .Q(\q_reg[15]__0_n_0 ));
  FDCE \q_reg[16] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[16]_i_1_n_0 ),
        .Q(\q_reg_n_0_[16] ));
  FDCE \q_reg[16]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1_n_7 ),
        .Q(\q_reg[16]__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[16]_i_1 
       (.CI(\q_reg[12]_i_1_n_0 ),
        .CO({\q_reg[16]_i_1_n_0 ,\q_reg[16]_i_1_n_1 ,\q_reg[16]_i_1_n_2 ,\q_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[16]_i_1_n_4 ,\q_reg[16]_i_1_n_5 ,\q_reg[16]_i_1_n_6 ,\q_reg[16]_i_1_n_7 }),
        .S({\q_reg[19]__0_n_0 ,\q_reg[18]__0_n_0 ,\q_reg[17]__0_n_0 ,\q_reg[16]__0_n_0 }));
  FDCE \q_reg[17] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[17]_i_1_n_0 ),
        .Q(\q_reg_n_0_[17] ));
  FDCE \q_reg[17]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1_n_6 ),
        .Q(\q_reg[17]__0_n_0 ));
  FDCE \q_reg[18] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[18]_i_1_n_0 ),
        .Q(\q_reg_n_0_[18] ));
  FDCE \q_reg[18]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1_n_5 ),
        .Q(\q_reg[18]__0_n_0 ));
  FDCE \q_reg[19] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[19]_i_1_n_0 ),
        .Q(\q_reg_n_0_[19] ));
  FDCE \q_reg[19]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1_n_4 ),
        .Q(\q_reg[19]__0_n_0 ));
  FDCE \q_reg[1] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[1]_i_1_n_0 ),
        .Q(\q_reg_n_0_[1] ));
  FDCE \q_reg[1]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[1]__0_i_1_n_0 ),
        .Q(\q_reg[1]__0_n_0 ));
  FDCE \q_reg[1]__1 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[1]__1_i_1_n_0 ),
        .Q(\q_reg[1]__1_0 ));
  FDCE \q_reg[1]__2 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1_n_6 ),
        .Q(\q_reg[1]__2_n_0 ));
  FDCE \q_reg[20] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[20]_i_1_n_0 ),
        .Q(\q_reg_n_0_[20] ));
  FDCE \q_reg[20]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1_n_7 ),
        .Q(\q_reg[20]__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[20]_i_1 
       (.CI(\q_reg[16]_i_1_n_0 ),
        .CO({\q_reg[20]_i_1_n_0 ,\q_reg[20]_i_1_n_1 ,\q_reg[20]_i_1_n_2 ,\q_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[20]_i_1_n_4 ,\q_reg[20]_i_1_n_5 ,\q_reg[20]_i_1_n_6 ,\q_reg[20]_i_1_n_7 }),
        .S({\q_reg[23]__0_n_0 ,\q_reg[22]__0_n_0 ,\q_reg[21]__0_n_0 ,\q_reg[20]__0_n_0 }));
  FDCE \q_reg[21] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[21]_i_1_n_0 ),
        .Q(\q_reg_n_0_[21] ));
  FDCE \q_reg[21]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1_n_6 ),
        .Q(\q_reg[21]__0_n_0 ));
  FDCE \q_reg[22] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[22]_i_1_n_0 ),
        .Q(\q_reg_n_0_[22] ));
  FDCE \q_reg[22]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1_n_5 ),
        .Q(\q_reg[22]__0_n_0 ));
  FDCE \q_reg[23] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[23]_i_1_n_0 ),
        .Q(\q_reg_n_0_[23] ));
  FDCE \q_reg[23]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1_n_4 ),
        .Q(\q_reg[23]__0_n_0 ));
  FDCE \q_reg[24] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[24]_i_1_n_0 ),
        .Q(\q_reg_n_0_[24] ));
  FDCE \q_reg[24]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1_n_7 ),
        .Q(\q_reg[24]__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[24]_i_1 
       (.CI(\q_reg[20]_i_1_n_0 ),
        .CO({\q_reg[24]_i_1_n_0 ,\q_reg[24]_i_1_n_1 ,\q_reg[24]_i_1_n_2 ,\q_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[24]_i_1_n_4 ,\q_reg[24]_i_1_n_5 ,\q_reg[24]_i_1_n_6 ,\q_reg[24]_i_1_n_7 }),
        .S({\q_reg[27]__0_n_0 ,\q_reg[26]__0_n_0 ,\q_reg[25]__0_n_0 ,\q_reg[24]__0_n_0 }));
  FDCE \q_reg[25] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[25]_i_1_n_0 ),
        .Q(\q_reg_n_0_[25] ));
  FDCE \q_reg[25]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1_n_6 ),
        .Q(\q_reg[25]__0_n_0 ));
  FDCE \q_reg[26] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[26]_i_1_n_0 ),
        .Q(\q_reg_n_0_[26] ));
  FDCE \q_reg[26]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1_n_5 ),
        .Q(\q_reg[26]__0_n_0 ));
  FDCE \q_reg[27] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[27]_i_1_n_0 ),
        .Q(\q_reg_n_0_[27] ));
  FDCE \q_reg[27]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1_n_4 ),
        .Q(\q_reg[27]__0_n_0 ));
  FDCE \q_reg[28] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[28]_i_1_n_0 ),
        .Q(p_0_in0));
  FDCE \q_reg[28]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[31]_i_1_n_7 ),
        .Q(led[0]));
  FDCE \q_reg[29] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[29]_i_1_n_0 ),
        .Q(\q_reg_n_0_[29] ));
  FDCE \q_reg[29]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[31]_i_1_n_6 ),
        .Q(led[1]));
  FDCE \q_reg[2] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[2]_i_1_n_0 ),
        .Q(\q_reg_n_0_[2] ));
  FDCE \q_reg[2]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[2]__0_i_1_n_0 ),
        .Q(\q_reg[2]__0_0 ));
  FDCE \q_reg[2]__1 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1_n_5 ),
        .Q(\q_reg[2]__1_n_0 ));
  FDCE \q_reg[30] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[30]_i_1_n_0 ),
        .Q(\q_reg_n_0_[30] ));
  FDCE \q_reg[30]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[31]_i_1_n_5 ),
        .Q(led[2]));
  FDCE \q_reg[31] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[31]_i_1_n_0 ),
        .Q(\q_reg_n_0_[31] ));
  FDCE \q_reg[31]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[31]_i_1_n_4 ),
        .Q(led[3]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[31]_i_1 
       (.CI(\q_reg[24]_i_1_n_0 ),
        .CO({\NLW_q_reg[31]_i_1_CO_UNCONNECTED [3],\q_reg[31]_i_1_n_1 ,\q_reg[31]_i_1_n_2 ,\q_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[31]_i_1_n_4 ,\q_reg[31]_i_1_n_5 ,\q_reg[31]_i_1_n_6 ,\q_reg[31]_i_1_n_7 }),
        .S(led));
  FDCE \q_reg[3] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[3]_i_1_n_0 ),
        .Q(\q_reg_n_0_[3] ));
  FDCE \q_reg[3]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[3]__0_i_1_n_0 ),
        .Q(\q_reg[3]__0_0 ));
  FDCE \q_reg[3]__1 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1_n_4 ),
        .Q(\q_reg[3]__1_n_0 ));
  FDCE \q_reg[4] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[4]_i_1_n_0 ),
        .Q(\q_reg_n_0_[4] ));
  FDCE \q_reg[4]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1_n_7 ),
        .Q(\q_reg[4]__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[4]_i_1 
       (.CI(\q_reg[0]_i_1_n_0 ),
        .CO({\q_reg[4]_i_1_n_0 ,\q_reg[4]_i_1_n_1 ,\q_reg[4]_i_1_n_2 ,\q_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[4]_i_1_n_4 ,\q_reg[4]_i_1_n_5 ,\q_reg[4]_i_1_n_6 ,\q_reg[4]_i_1_n_7 }),
        .S({\q_reg[7]__0_n_0 ,\q_reg[6]__0_n_0 ,\q_reg[5]__0_n_0 ,\q_reg[4]__0_n_0 }));
  FDCE \q_reg[5] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[5]_i_1_n_0 ),
        .Q(\q_reg_n_0_[5] ));
  FDCE \q_reg[5]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1_n_6 ),
        .Q(\q_reg[5]__0_n_0 ));
  FDCE \q_reg[6] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[6]_i_1_n_0 ),
        .Q(\q_reg_n_0_[6] ));
  FDCE \q_reg[6]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1_n_5 ),
        .Q(\q_reg[6]__0_n_0 ));
  FDCE \q_reg[7] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[7]_i_1_n_0 ),
        .Q(\q_reg_n_0_[7] ));
  FDCE \q_reg[7]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1_n_4 ),
        .Q(\q_reg[7]__0_n_0 ));
  FDCE \q_reg[8] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[8]_i_1_n_0 ),
        .Q(\q_reg_n_0_[8] ));
  FDCE \q_reg[8]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1_n_7 ),
        .Q(\q_reg[8]__0_n_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[8]_i_1 
       (.CI(\q_reg[4]_i_1_n_0 ),
        .CO({\q_reg[8]_i_1_n_0 ,\q_reg[8]_i_1_n_1 ,\q_reg[8]_i_1_n_2 ,\q_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[8]_i_1_n_4 ,\q_reg[8]_i_1_n_5 ,\q_reg[8]_i_1_n_6 ,\q_reg[8]_i_1_n_7 }),
        .S({\q_reg[11]__0_n_0 ,\q_reg[10]__0_n_0 ,\q_reg[9]__0_n_0 ,\q_reg[8]__0_n_0 }));
  FDCE \q_reg[9] 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q[9]_i_1_n_0 ),
        .Q(\q_reg_n_0_[9] ));
  FDCE \q_reg[9]__0 
       (.C(io_clk100),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1_n_6 ),
        .Q(\q_reg[9]__0_n_0 ));
endmodule

(* ORIG_REF_NAME = "tis100_top" *) 
module block_top_tis100_top_wrap_0_0_tis100_top
   (q_reg_0,
    q_reg__0_0,
    q_reg__3_0,
    q_reg__4_0,
    q_reg__6_0,
    s_axi_lite_rvalid,
    q_reg__2_0,
    q_reg__5_0,
    s_axi_lite_bvalid,
    q_reg__8_0,
    s_axi_lite_rresp,
    s_axi_lite_bresp,
    \q_reg[3]__0_0 ,
    \q_reg[2]__0_0 ,
    \q_reg[1]__1_0 ,
    \q_reg[0]__1_0 ,
    Q,
    \q_reg[8]__1_0 ,
    s_axi_lite_rdata,
    \q_reg[1]__1_1 ,
    led,
    d015_out,
    \q_reg[8]_0 ,
    \q_reg[2]_0 ,
    s_axi_lite_rready,
    clock,
    s_axi_lite_arvalid,
    s_axi_lite_bready,
    s_axi_lite_awvalid,
    s_axi_lite_wvalid,
    q_reg__1_0,
    \q_reg[31]__0_0 ,
    q_reg__2_1,
    q_reg__5_1,
    q_reg__7_0,
    q_reg__8_1,
    \q_reg[1]__5__0_0 ,
    \q_reg[1]__6_0 ,
    io_clk100,
    pb,
    s_axi_lite_araddr,
    s_axi_lite_awaddr,
    s_axi_lite_wdata,
    dip);
  output q_reg_0;
  output q_reg__0_0;
  output q_reg__3_0;
  output q_reg__4_0;
  output q_reg__6_0;
  output s_axi_lite_rvalid;
  output q_reg__2_0;
  output q_reg__5_0;
  output s_axi_lite_bvalid;
  output q_reg__8_0;
  output [0:0]s_axi_lite_rresp;
  output [0:0]s_axi_lite_bresp;
  output \q_reg[3]__0_0 ;
  output \q_reg[2]__0_0 ;
  output \q_reg[1]__1_0 ;
  output \q_reg[0]__1_0 ;
  output [1:0]Q;
  output [1:0]\q_reg[8]__1_0 ;
  output [31:0]s_axi_lite_rdata;
  output \q_reg[1]__1_1 ;
  output [3:0]led;
  output d015_out;
  output \q_reg[8]_0 ;
  output \q_reg[2]_0 ;
  input s_axi_lite_rready;
  input clock;
  input s_axi_lite_arvalid;
  input s_axi_lite_bready;
  input s_axi_lite_awvalid;
  input s_axi_lite_wvalid;
  input q_reg__1_0;
  input \q_reg[31]__0_0 ;
  input q_reg__2_1;
  input q_reg__5_1;
  input q_reg__7_0;
  input q_reg__8_1;
  input \q_reg[1]__5__0_0 ;
  input \q_reg[1]__6_0 ;
  input io_clk100;
  input [3:0]pb;
  input [9:0]s_axi_lite_araddr;
  input [9:0]s_axi_lite_awaddr;
  input [31:0]s_axi_lite_wdata;
  input [3:0]dip;

  wire [1:0]Q;
  wire clock;
  wire d0;
  wire d015_out;
  wire [3:0]dip;
  wire io_clk100;
  wire [3:0]led;
  wire [31:0]p_0_in;
  wire p_0_in0;
  wire [3:0]pb;
  wire \q[0]_i_2__0_n_0 ;
  wire \q[31]__0_i_1_n_0 ;
  wire \q[31]__0_i_2_n_0 ;
  wire \q[31]__0_i_3_n_0 ;
  wire \q[31]__1_i_1_n_0 ;
  wire \q_reg[0]__0_n_0 ;
  wire \q_reg[0]__1_0 ;
  wire \q_reg[0]__1_n_0 ;
  wire \q_reg[0]__2_n_0 ;
  wire \q_reg[0]__3_n_0 ;
  wire \q_reg[0]_i_1__0_n_0 ;
  wire \q_reg[0]_i_1__0_n_1 ;
  wire \q_reg[0]_i_1__0_n_2 ;
  wire \q_reg[0]_i_1__0_n_3 ;
  wire \q_reg[0]_i_1__0_n_4 ;
  wire \q_reg[0]_i_1__0_n_5 ;
  wire \q_reg[0]_i_1__0_n_6 ;
  wire \q_reg[0]_i_1__0_n_7 ;
  wire \q_reg[10]__0_n_0 ;
  wire \q_reg[11]__0_n_0 ;
  wire \q_reg[12]__0_n_0 ;
  wire \q_reg[12]_i_1__0_n_0 ;
  wire \q_reg[12]_i_1__0_n_1 ;
  wire \q_reg[12]_i_1__0_n_2 ;
  wire \q_reg[12]_i_1__0_n_3 ;
  wire \q_reg[12]_i_1__0_n_4 ;
  wire \q_reg[12]_i_1__0_n_5 ;
  wire \q_reg[12]_i_1__0_n_6 ;
  wire \q_reg[12]_i_1__0_n_7 ;
  wire \q_reg[13]__0_n_0 ;
  wire \q_reg[14]__0_n_0 ;
  wire \q_reg[15]__0_n_0 ;
  wire \q_reg[16]__0_n_0 ;
  wire \q_reg[16]_i_1__0_n_0 ;
  wire \q_reg[16]_i_1__0_n_1 ;
  wire \q_reg[16]_i_1__0_n_2 ;
  wire \q_reg[16]_i_1__0_n_3 ;
  wire \q_reg[16]_i_1__0_n_4 ;
  wire \q_reg[16]_i_1__0_n_5 ;
  wire \q_reg[16]_i_1__0_n_6 ;
  wire \q_reg[16]_i_1__0_n_7 ;
  wire \q_reg[17]__0_n_0 ;
  wire \q_reg[18]__0_n_0 ;
  wire \q_reg[19]__0_n_0 ;
  wire \q_reg[1]__0_n_0 ;
  wire \q_reg[1]__1_0 ;
  wire \q_reg[1]__1_1 ;
  wire \q_reg[1]__1_n_0 ;
  wire \q_reg[1]__2_n_0 ;
  wire \q_reg[1]__3_n_0 ;
  wire \q_reg[1]__5__0_0 ;
  wire \q_reg[1]__6_0 ;
  wire \q_reg[20]__0_n_0 ;
  wire \q_reg[20]_i_1__0_n_0 ;
  wire \q_reg[20]_i_1__0_n_1 ;
  wire \q_reg[20]_i_1__0_n_2 ;
  wire \q_reg[20]_i_1__0_n_3 ;
  wire \q_reg[20]_i_1__0_n_4 ;
  wire \q_reg[20]_i_1__0_n_5 ;
  wire \q_reg[20]_i_1__0_n_6 ;
  wire \q_reg[20]_i_1__0_n_7 ;
  wire \q_reg[21]__0_n_0 ;
  wire \q_reg[22]__0_n_0 ;
  wire \q_reg[23]__0_n_0 ;
  wire \q_reg[24]__0_n_0 ;
  wire \q_reg[24]_i_1__0_n_0 ;
  wire \q_reg[24]_i_1__0_n_1 ;
  wire \q_reg[24]_i_1__0_n_2 ;
  wire \q_reg[24]_i_1__0_n_3 ;
  wire \q_reg[24]_i_1__0_n_4 ;
  wire \q_reg[24]_i_1__0_n_5 ;
  wire \q_reg[24]_i_1__0_n_6 ;
  wire \q_reg[24]_i_1__0_n_7 ;
  wire \q_reg[25]__0_n_0 ;
  wire \q_reg[26]__0_n_0 ;
  wire \q_reg[27]__0_n_0 ;
  wire \q_reg[28]__0_n_0 ;
  wire \q_reg[28]_i_1_n_1 ;
  wire \q_reg[28]_i_1_n_2 ;
  wire \q_reg[28]_i_1_n_3 ;
  wire \q_reg[28]_i_1_n_4 ;
  wire \q_reg[28]_i_1_n_5 ;
  wire \q_reg[28]_i_1_n_6 ;
  wire \q_reg[28]_i_1_n_7 ;
  wire \q_reg[29]__0_n_0 ;
  wire \q_reg[2]_0 ;
  wire \q_reg[2]__0_0 ;
  wire \q_reg[2]__0_n_0 ;
  wire \q_reg[2]__2_n_0 ;
  wire \q_reg[2]__3_n_0 ;
  wire \q_reg[30]__0_n_0 ;
  wire \q_reg[31]__0_0 ;
  wire \q_reg[31]__0_n_0 ;
  wire \q_reg[3]__0_0 ;
  wire \q_reg[3]__0_n_0 ;
  wire \q_reg[3]__1_n_0 ;
  wire \q_reg[3]__2_n_0 ;
  wire \q_reg[3]__3_n_0 ;
  wire \q_reg[4]__0_n_0 ;
  wire \q_reg[4]__1_n_0 ;
  wire \q_reg[4]__2_n_0 ;
  wire \q_reg[4]__3_n_0 ;
  wire \q_reg[4]_i_1__0_n_0 ;
  wire \q_reg[4]_i_1__0_n_1 ;
  wire \q_reg[4]_i_1__0_n_2 ;
  wire \q_reg[4]_i_1__0_n_3 ;
  wire \q_reg[4]_i_1__0_n_4 ;
  wire \q_reg[4]_i_1__0_n_5 ;
  wire \q_reg[4]_i_1__0_n_6 ;
  wire \q_reg[4]_i_1__0_n_7 ;
  wire \q_reg[5]__0_n_0 ;
  wire \q_reg[5]__1_n_0 ;
  wire \q_reg[5]__2_n_0 ;
  wire \q_reg[5]__3_n_0 ;
  wire \q_reg[6]__0_n_0 ;
  wire \q_reg[6]__1_n_0 ;
  wire \q_reg[6]__2_n_0 ;
  wire \q_reg[6]__3_n_0 ;
  wire \q_reg[7]__0_n_0 ;
  wire \q_reg[7]__1_n_0 ;
  wire \q_reg[7]__2_n_0 ;
  wire \q_reg[7]__3_n_0 ;
  wire \q_reg[8]_0 ;
  wire \q_reg[8]__0_n_0 ;
  wire [1:0]\q_reg[8]__1_0 ;
  wire \q_reg[8]__2_n_0 ;
  wire \q_reg[8]__3_n_0 ;
  wire \q_reg[8]_i_1__0_n_0 ;
  wire \q_reg[8]_i_1__0_n_1 ;
  wire \q_reg[8]_i_1__0_n_2 ;
  wire \q_reg[8]_i_1__0_n_3 ;
  wire \q_reg[8]_i_1__0_n_4 ;
  wire \q_reg[8]_i_1__0_n_5 ;
  wire \q_reg[8]_i_1__0_n_6 ;
  wire \q_reg[8]_i_1__0_n_7 ;
  wire \q_reg[9]__0_n_0 ;
  wire \q_reg[9]__1_n_0 ;
  wire \q_reg[9]__2_n_0 ;
  wire \q_reg[9]__3_n_0 ;
  wire q_reg_0;
  wire q_reg__0_0;
  wire q_reg__1_0;
  wire q_reg__2_0;
  wire q_reg__2_1;
  wire q_reg__3_0;
  wire q_reg__4_0;
  wire q_reg__5_0;
  wire q_reg__5_1;
  wire q_reg__6_0;
  wire q_reg__7_0;
  wire q_reg__8_0;
  wire q_reg__8_1;
  wire [31:0]q_reg__9;
  wire \q_reg_n_0_[10] ;
  wire \q_reg_n_0_[11] ;
  wire \q_reg_n_0_[12] ;
  wire \q_reg_n_0_[13] ;
  wire \q_reg_n_0_[14] ;
  wire \q_reg_n_0_[15] ;
  wire \q_reg_n_0_[16] ;
  wire \q_reg_n_0_[17] ;
  wire \q_reg_n_0_[18] ;
  wire \q_reg_n_0_[19] ;
  wire \q_reg_n_0_[20] ;
  wire \q_reg_n_0_[21] ;
  wire \q_reg_n_0_[22] ;
  wire \q_reg_n_0_[23] ;
  wire \q_reg_n_0_[24] ;
  wire \q_reg_n_0_[25] ;
  wire \q_reg_n_0_[26] ;
  wire \q_reg_n_0_[27] ;
  wire \q_reg_n_0_[28] ;
  wire \q_reg_n_0_[29] ;
  wire \q_reg_n_0_[2] ;
  wire \q_reg_n_0_[30] ;
  wire \q_reg_n_0_[31] ;
  wire \q_reg_n_0_[3] ;
  wire \q_reg_n_0_[5] ;
  wire \q_reg_n_0_[6] ;
  wire \q_reg_n_0_[7] ;
  wire \q_reg_n_0_[8] ;
  wire \q_reg_n_0_[9] ;
  wire [9:0]s_axi_lite_araddr;
  wire s_axi_lite_arvalid;
  wire [9:0]s_axi_lite_awaddr;
  wire s_axi_lite_awvalid;
  wire s_axi_lite_bready;
  wire [0:0]s_axi_lite_bresp;
  wire s_axi_lite_bvalid;
  wire [31:0]s_axi_lite_rdata;
  wire s_axi_lite_rready;
  wire [0:0]s_axi_lite_rresp;
  wire s_axi_lite_rvalid;
  wire [31:0]s_axi_lite_wdata;
  wire s_axi_lite_wvalid;
  wire [3:3]\NLW_q_reg[28]_i_1_CO_UNCONNECTED ;

  block_top_tis100_top_wrap_0_0_led_game game
       (.dip(dip),
        .io_clk100(io_clk100),
        .led(led),
        .pb(pb),
        .\q_reg[0]__1_0 (\q_reg[0]__1_0 ),
        .\q_reg[1]__1_0 (\q_reg[1]__1_0 ),
        .\q_reg[2]__0_0 (\q_reg[2]__0_0 ),
        .\q_reg[31]__0_0 (\q_reg[31]__0_0 ),
        .\q_reg[3]__0_0 (\q_reg[3]__0_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hDD0D)) 
    \q[0]__4_i_1 
       (.I0(\q_reg_n_0_[2] ),
        .I1(\q_reg[0]__3_n_0 ),
        .I2(\q_reg_n_0_[3] ),
        .I3(q_reg__9[0]),
        .O(p_0_in[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \q[0]_i_2__0 
       (.I0(q_reg__9[0]),
        .O(\q[0]_i_2__0_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[10]__1_i_1 
       (.I0(\q_reg[10]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[10]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[10]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[11]__1_i_1 
       (.I0(\q_reg[11]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[11]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[11]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[12]__1_i_1 
       (.I0(\q_reg[12]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[12]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[12]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[13]__1_i_1 
       (.I0(\q_reg[13]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[13]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[13]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[14]__1_i_1 
       (.I0(\q_reg[14]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[14]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[14]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[15]__1_i_1 
       (.I0(\q_reg[15]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[15]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[15]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[16]__1_i_1 
       (.I0(\q_reg[16]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[16]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[16]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[17]__1_i_1 
       (.I0(\q_reg[17]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[17]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[17]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[18]__1_i_1 
       (.I0(\q_reg[18]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[18]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[18]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[19]__1_i_1 
       (.I0(\q_reg[19]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[19]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[19]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[1]__4_i_1 
       (.I0(\q_reg[1]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[1]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \q[1]__5_i_2 
       (.I0(\q_reg_n_0_[2] ),
        .I1(\q_reg_n_0_[3] ),
        .O(\q_reg[2]_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \q[1]__6_i_2 
       (.I0(\q[31]__0_i_2_n_0 ),
        .I1(\q_reg[1]__1_n_0 ),
        .I2(\q_reg[0]__1_n_0 ),
        .I3(\q_reg[3]__1_n_0 ),
        .I4(\q_reg[9]__1_n_0 ),
        .O(\q_reg[1]__1_1 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[20]__1_i_1 
       (.I0(\q_reg[20]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[20]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[20]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[21]__1_i_1 
       (.I0(\q_reg[21]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[21]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[21]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[22]__1_i_1 
       (.I0(\q_reg[22]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[22]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[22]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[23]__1_i_1 
       (.I0(\q_reg[23]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[23]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[23]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[24]__1_i_1 
       (.I0(\q_reg[24]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[24]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[24]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[25]__1_i_1 
       (.I0(\q_reg[25]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[25]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[25]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[26]__1_i_1 
       (.I0(\q_reg[26]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[26]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[26]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[27]__1_i_1 
       (.I0(\q_reg[27]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[27]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[27]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[28]__1_i_1 
       (.I0(\q_reg[28]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[28]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[28]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[29]__1_i_1 
       (.I0(\q_reg[29]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[29]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[29]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[2]__4_i_1 
       (.I0(\q_reg[2]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[2]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[2]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[30]__1_i_1 
       (.I0(\q_reg[30]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[30]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[30]));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \q[31]__0_i_1 
       (.I0(q_reg__5_0),
        .I1(q_reg__6_0),
        .I2(\q[31]__0_i_2_n_0 ),
        .I3(\q[31]__0_i_3_n_0 ),
        .I4(\q_reg[8]__1_0 [1]),
        .I5(\q_reg[9]__1_n_0 ),
        .O(\q[31]__0_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[31]__0_i_2 
       (.I0(\q_reg[6]__1_n_0 ),
        .I1(\q_reg[7]__1_n_0 ),
        .I2(\q_reg[4]__1_n_0 ),
        .I3(\q_reg[5]__1_n_0 ),
        .O(\q[31]__0_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \q[31]__0_i_3 
       (.I0(\q_reg[0]__1_n_0 ),
        .I1(\q_reg[3]__1_n_0 ),
        .I2(\q_reg[8]__1_0 [0]),
        .I3(\q_reg[1]__1_n_0 ),
        .O(\q[31]__0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000222)) 
    \q[31]__1_i_1 
       (.I0(d015_out),
        .I1(\q_reg[8]_0 ),
        .I2(\q_reg_n_0_[3] ),
        .I3(\q_reg_n_0_[2] ),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\q[31]__1_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[31]__1_i_2 
       (.I0(\q_reg[31]__0_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[31]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[31]));
  LUT2 #(
    .INIT(4'h8)) 
    \q[31]__1_i_3 
       (.I0(q_reg__0_0),
        .I1(q_reg__2_0),
        .O(d015_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \q[31]__1_i_4 
       (.I0(\q_reg_n_0_[8] ),
        .I1(\q_reg_n_0_[9] ),
        .I2(\q_reg_n_0_[5] ),
        .I3(p_0_in0),
        .I4(\q_reg_n_0_[7] ),
        .I5(\q_reg_n_0_[6] ),
        .O(\q_reg[8]_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[3]__4_i_1 
       (.I0(\q_reg[3]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[3]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[3]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[4]__4_i_1 
       (.I0(\q_reg[4]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[4]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[4]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[5]__4_i_1 
       (.I0(\q_reg[5]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[5]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[5]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[6]__4_i_1 
       (.I0(\q_reg[6]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[6]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[6]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[7]__4_i_1 
       (.I0(\q_reg[7]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[7]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[7]));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[8]__4_i_1 
       (.I0(\q_reg[8]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[8]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[8]));
  LUT2 #(
    .INIT(4'h8)) 
    \q[9]__1_i_1 
       (.I0(q_reg__4_0),
        .I1(q_reg__8_0),
        .O(d0));
  LUT4 #(
    .INIT(16'hF888)) 
    \q[9]__4_i_1 
       (.I0(\q_reg[9]__3_n_0 ),
        .I1(\q_reg_n_0_[2] ),
        .I2(q_reg__9[9]),
        .I3(\q_reg_n_0_[3] ),
        .O(p_0_in[9]));
  FDRE q_reg
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_rready),
        .Q(q_reg_0),
        .R(1'b0));
  FDRE \q_reg[0] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \q_reg[0]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[0]),
        .Q(\q_reg[0]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[0]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[0]__0_n_0 ),
        .Q(\q_reg[0]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[0]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[0]),
        .Q(\q_reg[0]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[0]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]__2_n_0 ),
        .Q(\q_reg[0]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[0]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[0]),
        .Q(s_axi_lite_rdata[0]));
  FDCE \q_reg[0]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1__0_n_7 ),
        .Q(q_reg__9[0]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[0]_i_1__0 
       (.CI(1'b0),
        .CO({\q_reg[0]_i_1__0_n_0 ,\q_reg[0]_i_1__0_n_1 ,\q_reg[0]_i_1__0_n_2 ,\q_reg[0]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\q_reg[0]_i_1__0_n_4 ,\q_reg[0]_i_1__0_n_5 ,\q_reg[0]_i_1__0_n_6 ,\q_reg[0]_i_1__0_n_7 }),
        .S({q_reg__9[3:1],\q[0]_i_2__0_n_0 }));
  FDRE \q_reg[10] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[10]),
        .Q(\q_reg_n_0_[10] ),
        .R(1'b0));
  FDCE \q_reg[10]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[10] ),
        .Q(\q_reg[10]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[10]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[10]),
        .Q(s_axi_lite_rdata[10]));
  FDCE \q_reg[10]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1__0_n_5 ),
        .Q(q_reg__9[10]));
  FDRE \q_reg[11] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[11]),
        .Q(\q_reg_n_0_[11] ),
        .R(1'b0));
  FDCE \q_reg[11]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[11] ),
        .Q(\q_reg[11]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[11]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[11]),
        .Q(s_axi_lite_rdata[11]));
  FDCE \q_reg[11]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1__0_n_4 ),
        .Q(q_reg__9[11]));
  FDRE \q_reg[12] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[12]),
        .Q(\q_reg_n_0_[12] ),
        .R(1'b0));
  FDCE \q_reg[12]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[12] ),
        .Q(\q_reg[12]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[12]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[12]),
        .Q(s_axi_lite_rdata[12]));
  FDCE \q_reg[12]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1__0_n_7 ),
        .Q(q_reg__9[12]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[12]_i_1__0 
       (.CI(\q_reg[8]_i_1__0_n_0 ),
        .CO({\q_reg[12]_i_1__0_n_0 ,\q_reg[12]_i_1__0_n_1 ,\q_reg[12]_i_1__0_n_2 ,\q_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[12]_i_1__0_n_4 ,\q_reg[12]_i_1__0_n_5 ,\q_reg[12]_i_1__0_n_6 ,\q_reg[12]_i_1__0_n_7 }),
        .S(q_reg__9[15:12]));
  FDRE \q_reg[13] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[13]),
        .Q(\q_reg_n_0_[13] ),
        .R(1'b0));
  FDCE \q_reg[13]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[13] ),
        .Q(\q_reg[13]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[13]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[13]),
        .Q(s_axi_lite_rdata[13]));
  FDCE \q_reg[13]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1__0_n_6 ),
        .Q(q_reg__9[13]));
  FDRE \q_reg[14] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[14]),
        .Q(\q_reg_n_0_[14] ),
        .R(1'b0));
  FDCE \q_reg[14]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[14] ),
        .Q(\q_reg[14]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[14]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[14]),
        .Q(s_axi_lite_rdata[14]));
  FDCE \q_reg[14]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1__0_n_5 ),
        .Q(q_reg__9[14]));
  FDRE \q_reg[15] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[15]),
        .Q(\q_reg_n_0_[15] ),
        .R(1'b0));
  FDCE \q_reg[15]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[15] ),
        .Q(\q_reg[15]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[15]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[15]),
        .Q(s_axi_lite_rdata[15]));
  FDCE \q_reg[15]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[12]_i_1__0_n_4 ),
        .Q(q_reg__9[15]));
  FDRE \q_reg[16] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[16]),
        .Q(\q_reg_n_0_[16] ),
        .R(1'b0));
  FDCE \q_reg[16]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[16] ),
        .Q(\q_reg[16]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[16]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[16]),
        .Q(s_axi_lite_rdata[16]));
  FDCE \q_reg[16]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1__0_n_7 ),
        .Q(q_reg__9[16]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[16]_i_1__0 
       (.CI(\q_reg[12]_i_1__0_n_0 ),
        .CO({\q_reg[16]_i_1__0_n_0 ,\q_reg[16]_i_1__0_n_1 ,\q_reg[16]_i_1__0_n_2 ,\q_reg[16]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[16]_i_1__0_n_4 ,\q_reg[16]_i_1__0_n_5 ,\q_reg[16]_i_1__0_n_6 ,\q_reg[16]_i_1__0_n_7 }),
        .S(q_reg__9[19:16]));
  FDRE \q_reg[17] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[17]),
        .Q(\q_reg_n_0_[17] ),
        .R(1'b0));
  FDCE \q_reg[17]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[17] ),
        .Q(\q_reg[17]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[17]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[17]),
        .Q(s_axi_lite_rdata[17]));
  FDCE \q_reg[17]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1__0_n_6 ),
        .Q(q_reg__9[17]));
  FDRE \q_reg[18] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[18]),
        .Q(\q_reg_n_0_[18] ),
        .R(1'b0));
  FDCE \q_reg[18]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[18] ),
        .Q(\q_reg[18]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[18]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[18]),
        .Q(s_axi_lite_rdata[18]));
  FDCE \q_reg[18]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1__0_n_5 ),
        .Q(q_reg__9[18]));
  FDRE \q_reg[19] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[19]),
        .Q(\q_reg_n_0_[19] ),
        .R(1'b0));
  FDCE \q_reg[19]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[19] ),
        .Q(\q_reg[19]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[19]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[19]),
        .Q(s_axi_lite_rdata[19]));
  FDCE \q_reg[19]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[16]_i_1__0_n_4 ),
        .Q(q_reg__9[19]));
  FDRE \q_reg[1] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \q_reg[1]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[1]),
        .Q(\q_reg[1]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[1]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[1]__0_n_0 ),
        .Q(\q_reg[1]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[1]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[1]),
        .Q(\q_reg[1]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[1]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[1]__2_n_0 ),
        .Q(\q_reg[1]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[1]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[1]),
        .Q(s_axi_lite_rdata[1]));
  FDCE \q_reg[1]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1__0_n_6 ),
        .Q(q_reg__9[1]));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RRESP" *) 
  FDCE \q_reg[1]__5__0 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[1]__5__0_0 ),
        .Q(s_axi_lite_rresp));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BRESP" *) 
  FDCE \q_reg[1]__6 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[1]__6_0 ),
        .Q(s_axi_lite_bresp));
  FDRE \q_reg[20] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[20]),
        .Q(\q_reg_n_0_[20] ),
        .R(1'b0));
  FDCE \q_reg[20]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[20] ),
        .Q(\q_reg[20]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[20]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[20]),
        .Q(s_axi_lite_rdata[20]));
  FDCE \q_reg[20]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1__0_n_7 ),
        .Q(q_reg__9[20]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[20]_i_1__0 
       (.CI(\q_reg[16]_i_1__0_n_0 ),
        .CO({\q_reg[20]_i_1__0_n_0 ,\q_reg[20]_i_1__0_n_1 ,\q_reg[20]_i_1__0_n_2 ,\q_reg[20]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[20]_i_1__0_n_4 ,\q_reg[20]_i_1__0_n_5 ,\q_reg[20]_i_1__0_n_6 ,\q_reg[20]_i_1__0_n_7 }),
        .S(q_reg__9[23:20]));
  FDRE \q_reg[21] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[21]),
        .Q(\q_reg_n_0_[21] ),
        .R(1'b0));
  FDCE \q_reg[21]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[21] ),
        .Q(\q_reg[21]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[21]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[21]),
        .Q(s_axi_lite_rdata[21]));
  FDCE \q_reg[21]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1__0_n_6 ),
        .Q(q_reg__9[21]));
  FDRE \q_reg[22] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[22]),
        .Q(\q_reg_n_0_[22] ),
        .R(1'b0));
  FDCE \q_reg[22]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[22] ),
        .Q(\q_reg[22]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[22]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[22]),
        .Q(s_axi_lite_rdata[22]));
  FDCE \q_reg[22]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1__0_n_5 ),
        .Q(q_reg__9[22]));
  FDRE \q_reg[23] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[23]),
        .Q(\q_reg_n_0_[23] ),
        .R(1'b0));
  FDCE \q_reg[23]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[23] ),
        .Q(\q_reg[23]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[23]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[23]),
        .Q(s_axi_lite_rdata[23]));
  FDCE \q_reg[23]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[20]_i_1__0_n_4 ),
        .Q(q_reg__9[23]));
  FDRE \q_reg[24] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[24]),
        .Q(\q_reg_n_0_[24] ),
        .R(1'b0));
  FDCE \q_reg[24]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[24] ),
        .Q(\q_reg[24]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[24]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[24]),
        .Q(s_axi_lite_rdata[24]));
  FDCE \q_reg[24]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1__0_n_7 ),
        .Q(q_reg__9[24]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[24]_i_1__0 
       (.CI(\q_reg[20]_i_1__0_n_0 ),
        .CO({\q_reg[24]_i_1__0_n_0 ,\q_reg[24]_i_1__0_n_1 ,\q_reg[24]_i_1__0_n_2 ,\q_reg[24]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[24]_i_1__0_n_4 ,\q_reg[24]_i_1__0_n_5 ,\q_reg[24]_i_1__0_n_6 ,\q_reg[24]_i_1__0_n_7 }),
        .S(q_reg__9[27:24]));
  FDRE \q_reg[25] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[25]),
        .Q(\q_reg_n_0_[25] ),
        .R(1'b0));
  FDCE \q_reg[25]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[25] ),
        .Q(\q_reg[25]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[25]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[25]),
        .Q(s_axi_lite_rdata[25]));
  FDCE \q_reg[25]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1__0_n_6 ),
        .Q(q_reg__9[25]));
  FDRE \q_reg[26] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[26]),
        .Q(\q_reg_n_0_[26] ),
        .R(1'b0));
  FDCE \q_reg[26]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[26] ),
        .Q(\q_reg[26]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[26]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[26]),
        .Q(s_axi_lite_rdata[26]));
  FDCE \q_reg[26]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1__0_n_5 ),
        .Q(q_reg__9[26]));
  FDRE \q_reg[27] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[27]),
        .Q(\q_reg_n_0_[27] ),
        .R(1'b0));
  FDCE \q_reg[27]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[27] ),
        .Q(\q_reg[27]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[27]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[27]),
        .Q(s_axi_lite_rdata[27]));
  FDCE \q_reg[27]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[24]_i_1__0_n_4 ),
        .Q(q_reg__9[27]));
  FDRE \q_reg[28] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[28]),
        .Q(\q_reg_n_0_[28] ),
        .R(1'b0));
  FDCE \q_reg[28]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[28] ),
        .Q(\q_reg[28]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[28]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[28]),
        .Q(s_axi_lite_rdata[28]));
  FDCE \q_reg[28]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[28]_i_1_n_7 ),
        .Q(q_reg__9[28]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[28]_i_1 
       (.CI(\q_reg[24]_i_1__0_n_0 ),
        .CO({\NLW_q_reg[28]_i_1_CO_UNCONNECTED [3],\q_reg[28]_i_1_n_1 ,\q_reg[28]_i_1_n_2 ,\q_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[28]_i_1_n_4 ,\q_reg[28]_i_1_n_5 ,\q_reg[28]_i_1_n_6 ,\q_reg[28]_i_1_n_7 }),
        .S(q_reg__9[31:28]));
  FDRE \q_reg[29] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[29]),
        .Q(\q_reg_n_0_[29] ),
        .R(1'b0));
  FDCE \q_reg[29]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[29] ),
        .Q(\q_reg[29]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[29]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[29]),
        .Q(s_axi_lite_rdata[29]));
  FDCE \q_reg[29]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[28]_i_1_n_6 ),
        .Q(q_reg__9[29]));
  FDRE \q_reg[2] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[2]),
        .Q(\q_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \q_reg[2]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[2]),
        .Q(\q_reg[2]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[2]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[2]__0_n_0 ),
        .Q(\q_reg[8]__1_0 [0]),
        .R(1'b0));
  FDRE \q_reg[2]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[2]),
        .Q(\q_reg[2]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[2]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[2]__2_n_0 ),
        .Q(\q_reg[2]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[2]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[2]),
        .Q(s_axi_lite_rdata[2]));
  FDCE \q_reg[2]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1__0_n_5 ),
        .Q(q_reg__9[2]));
  FDRE \q_reg[30] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[30]),
        .Q(\q_reg_n_0_[30] ),
        .R(1'b0));
  FDCE \q_reg[30]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[30] ),
        .Q(\q_reg[30]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[30]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[30]),
        .Q(s_axi_lite_rdata[30]));
  FDCE \q_reg[30]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[28]_i_1_n_5 ),
        .Q(q_reg__9[30]));
  FDRE \q_reg[31] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[31]),
        .Q(\q_reg_n_0_[31] ),
        .R(1'b0));
  FDCE \q_reg[31]__0 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg_n_0_[31] ),
        .Q(\q_reg[31]__0_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[31]__1 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[31]),
        .Q(s_axi_lite_rdata[31]));
  FDCE \q_reg[31]__2 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[28]_i_1_n_4 ),
        .Q(q_reg__9[31]));
  FDRE \q_reg[3] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[3]),
        .Q(\q_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \q_reg[3]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[3]),
        .Q(\q_reg[3]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[3]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[3]__0_n_0 ),
        .Q(\q_reg[3]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[3]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[3]),
        .Q(\q_reg[3]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[3]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[3]__2_n_0 ),
        .Q(\q_reg[3]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[3]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[3]),
        .Q(s_axi_lite_rdata[3]));
  FDCE \q_reg[3]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[0]_i_1__0_n_4 ),
        .Q(q_reg__9[3]));
  FDRE \q_reg[4] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[4]),
        .Q(p_0_in0),
        .R(1'b0));
  FDRE \q_reg[4]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[4]),
        .Q(\q_reg[4]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[4]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[4]__0_n_0 ),
        .Q(\q_reg[4]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[4]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[4]),
        .Q(\q_reg[4]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[4]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]__2_n_0 ),
        .Q(\q_reg[4]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[4]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[4]),
        .Q(s_axi_lite_rdata[4]));
  FDCE \q_reg[4]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1__0_n_7 ),
        .Q(q_reg__9[4]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[4]_i_1__0 
       (.CI(\q_reg[0]_i_1__0_n_0 ),
        .CO({\q_reg[4]_i_1__0_n_0 ,\q_reg[4]_i_1__0_n_1 ,\q_reg[4]_i_1__0_n_2 ,\q_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[4]_i_1__0_n_4 ,\q_reg[4]_i_1__0_n_5 ,\q_reg[4]_i_1__0_n_6 ,\q_reg[4]_i_1__0_n_7 }),
        .S(q_reg__9[7:4]));
  FDRE \q_reg[5] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[5]),
        .Q(\q_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \q_reg[5]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[5]),
        .Q(\q_reg[5]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[5]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[5]__0_n_0 ),
        .Q(\q_reg[5]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[5]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[5]),
        .Q(\q_reg[5]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[5]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[5]__2_n_0 ),
        .Q(\q_reg[5]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[5]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[5]),
        .Q(s_axi_lite_rdata[5]));
  FDCE \q_reg[5]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1__0_n_6 ),
        .Q(q_reg__9[5]));
  FDRE \q_reg[6] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[6]),
        .Q(\q_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \q_reg[6]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[6]),
        .Q(\q_reg[6]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[6]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[6]__0_n_0 ),
        .Q(\q_reg[6]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[6]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[6]),
        .Q(\q_reg[6]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[6]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[6]__2_n_0 ),
        .Q(\q_reg[6]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[6]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[6]),
        .Q(s_axi_lite_rdata[6]));
  FDCE \q_reg[6]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1__0_n_5 ),
        .Q(q_reg__9[6]));
  FDRE \q_reg[7] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[7]),
        .Q(\q_reg_n_0_[7] ),
        .R(1'b0));
  FDRE \q_reg[7]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[7]),
        .Q(\q_reg[7]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[7]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[7]__0_n_0 ),
        .Q(\q_reg[7]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[7]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[7]),
        .Q(\q_reg[7]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[7]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[7]__2_n_0 ),
        .Q(\q_reg[7]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[7]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[7]),
        .Q(s_axi_lite_rdata[7]));
  FDCE \q_reg[7]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[4]_i_1__0_n_4 ),
        .Q(q_reg__9[7]));
  FDRE \q_reg[8] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[8]),
        .Q(\q_reg_n_0_[8] ),
        .R(1'b0));
  FDRE \q_reg[8]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[8]),
        .Q(\q_reg[8]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[8]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[8]__0_n_0 ),
        .Q(\q_reg[8]__1_0 [1]),
        .R(1'b0));
  FDRE \q_reg[8]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[8]),
        .Q(\q_reg[8]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[8]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]__2_n_0 ),
        .Q(\q_reg[8]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[8]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[8]),
        .Q(s_axi_lite_rdata[8]));
  FDCE \q_reg[8]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1__0_n_7 ),
        .Q(q_reg__9[8]));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[8]_i_1__0 
       (.CI(\q_reg[4]_i_1__0_n_0 ),
        .CO({\q_reg[8]_i_1__0_n_0 ,\q_reg[8]_i_1__0_n_1 ,\q_reg[8]_i_1__0_n_2 ,\q_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[8]_i_1__0_n_4 ,\q_reg[8]_i_1__0_n_5 ,\q_reg[8]_i_1__0_n_6 ,\q_reg[8]_i_1__0_n_7 }),
        .S(q_reg__9[11:8]));
  FDRE \q_reg[9] 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_araddr[9]),
        .Q(\q_reg_n_0_[9] ),
        .R(1'b0));
  FDRE \q_reg[9]__0 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awaddr[9]),
        .Q(\q_reg[9]__0_n_0 ),
        .R(1'b0));
  FDRE \q_reg[9]__1 
       (.C(clock),
        .CE(d0),
        .D(\q_reg[9]__0_n_0 ),
        .Q(\q_reg[9]__1_n_0 ),
        .R(1'b0));
  FDRE \q_reg[9]__2 
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wdata[9]),
        .Q(\q_reg[9]__2_n_0 ),
        .R(1'b0));
  FDCE \q_reg[9]__3 
       (.C(clock),
        .CE(\q[31]__0_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[9]__2_n_0 ),
        .Q(\q_reg[9]__3_n_0 ));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RDATA" *) 
  FDCE \q_reg[9]__4 
       (.C(clock),
        .CE(\q[31]__1_i_1_n_0 ),
        .CLR(\q_reg[31]__0_0 ),
        .D(p_0_in[9]),
        .Q(s_axi_lite_rdata[9]));
  FDCE \q_reg[9]__5 
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(\q_reg[8]_i_1__0_n_6 ),
        .Q(q_reg__9[9]));
  FDRE q_reg__0
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_arvalid),
        .Q(q_reg__0_0),
        .R(1'b0));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE RVALID" *) 
  FDCE q_reg__1
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(q_reg__1_0),
        .Q(s_axi_lite_rvalid));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE ARREADY" *) 
  FDPE q_reg__2
       (.C(clock),
        .CE(1'b1),
        .D(q_reg__2_1),
        .PRE(\q_reg[31]__0_0 ),
        .Q(q_reg__2_0));
  FDRE q_reg__3
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_bready),
        .Q(q_reg__3_0),
        .R(1'b0));
  FDRE q_reg__4
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_awvalid),
        .Q(q_reg__4_0),
        .R(1'b0));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE WREADY" *) 
  FDCE q_reg__5
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(q_reg__5_1),
        .Q(q_reg__5_0));
  FDRE q_reg__6
       (.C(clock),
        .CE(1'b1),
        .D(s_axi_lite_wvalid),
        .Q(q_reg__6_0),
        .R(1'b0));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE BVALID" *) 
  FDCE q_reg__7
       (.C(clock),
        .CE(1'b1),
        .CLR(\q_reg[31]__0_0 ),
        .D(q_reg__7_0),
        .Q(s_axi_lite_bvalid));
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm_rtl:1.0 S_AXI_LITE AWREADY" *) 
  FDPE q_reg__8
       (.C(clock),
        .CE(1'b1),
        .D(q_reg__8_1),
        .PRE(\q_reg[31]__0_0 ),
        .Q(q_reg__8_0));
endmodule

(* ORIG_REF_NAME = "tis100_top_wrap" *) 
module block_top_tis100_top_wrap_0_0_tis100_top_wrap
   (\q_reg[3]__0 ,
    \q_reg[2]__0 ,
    \q_reg[1]__1 ,
    \q_reg[0]__1 ,
    s_axi_lite_rdata,
    led,
    q_reg__2,
    q_reg__8,
    q_reg__5,
    s_axi_lite_bvalid,
    s_axi_lite_rresp,
    s_axi_lite_bresp,
    s_axi_lite_rvalid,
    io_clk100,
    pb,
    dip,
    s_axi_lite_rready,
    clock,
    s_axi_lite_arvalid,
    s_axi_lite_bready,
    s_axi_lite_awvalid,
    s_axi_lite_wvalid,
    s_axi_lite_araddr,
    s_axi_lite_awaddr,
    s_axi_lite_wdata,
    resetn);
  output \q_reg[3]__0 ;
  output \q_reg[2]__0 ;
  output \q_reg[1]__1 ;
  output \q_reg[0]__1 ;
  output [31:0]s_axi_lite_rdata;
  output [3:0]led;
  output q_reg__2;
  output q_reg__8;
  output q_reg__5;
  output s_axi_lite_bvalid;
  output [0:0]s_axi_lite_rresp;
  output [0:0]s_axi_lite_bresp;
  output s_axi_lite_rvalid;
  input io_clk100;
  input [3:0]pb;
  input [3:0]dip;
  input s_axi_lite_rready;
  input clock;
  input s_axi_lite_arvalid;
  input s_axi_lite_bready;
  input s_axi_lite_awvalid;
  input s_axi_lite_wvalid;
  input [9:0]s_axi_lite_araddr;
  input [9:0]s_axi_lite_awaddr;
  input [31:0]s_axi_lite_wdata;
  input resetn;

  wire clock;
  wire d015_out;
  wire [3:0]dip;
  wire io_clk100;
  wire [3:0]led;
  wire [3:0]pb;
  wire \q[1]__5_i_1_n_0 ;
  wire \q[1]__6_i_1_n_0 ;
  wire q__1_i_1_n_0;
  wire q__2_i_1_n_0;
  wire q__2_i_2_n_0;
  wire q__5_i_1_n_0;
  wire q__7_i_1_n_0;
  wire q__8_i_1_n_0;
  wire \q_reg[0]__1 ;
  wire \q_reg[1]__1 ;
  wire \q_reg[2]__0 ;
  wire \q_reg[3]__0 ;
  wire q_reg__2;
  wire q_reg__5;
  wire q_reg__8;
  wire resetn;
  wire [9:0]s_axi_lite_araddr;
  wire s_axi_lite_arvalid;
  wire [9:0]s_axi_lite_awaddr;
  wire s_axi_lite_awvalid;
  wire s_axi_lite_bready;
  wire [0:0]s_axi_lite_bresp;
  wire s_axi_lite_bvalid;
  wire [31:0]s_axi_lite_rdata;
  wire s_axi_lite_rready;
  wire [0:0]s_axi_lite_rresp;
  wire s_axi_lite_rvalid;
  wire [31:0]s_axi_lite_wdata;
  wire s_axi_lite_wvalid;
  wire tis100_n_0;
  wire tis100_n_1;
  wire tis100_n_16;
  wire tis100_n_17;
  wire tis100_n_18;
  wire tis100_n_19;
  wire tis100_n_2;
  wire tis100_n_3;
  wire tis100_n_4;
  wire tis100_n_52;
  wire tis100_n_58;
  wire tis100_n_59;

  LUT6 #(
    .INIT(64'hFFEFFFFFFFEF0000)) 
    \q[1]__5_i_1 
       (.I0(tis100_n_58),
        .I1(tis100_n_16),
        .I2(tis100_n_59),
        .I3(tis100_n_17),
        .I4(d015_out),
        .I5(s_axi_lite_rresp),
        .O(\q[1]__5_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFDFFFFFFFD000000)) 
    \q[1]__6_i_1 
       (.I0(tis100_n_19),
        .I1(tis100_n_18),
        .I2(tis100_n_52),
        .I3(tis100_n_4),
        .I4(q_reg__5),
        .I5(s_axi_lite_bresp),
        .O(\q[1]__6_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h7444)) 
    q__1_i_1
       (.I0(tis100_n_0),
        .I1(s_axi_lite_rvalid),
        .I2(q_reg__2),
        .I3(tis100_n_1),
        .O(q__1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h88F8)) 
    q__2_i_1
       (.I0(tis100_n_0),
        .I1(s_axi_lite_rvalid),
        .I2(q_reg__2),
        .I3(tis100_n_1),
        .O(q__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q__2_i_2
       (.I0(resetn),
        .O(q__2_i_2_n_0));
  LUT4 #(
    .INIT(16'h7222)) 
    q__5_i_1
       (.I0(q_reg__5),
        .I1(tis100_n_4),
        .I2(q_reg__8),
        .I3(tis100_n_3),
        .O(q__5_i_1_n_0));
  LUT4 #(
    .INIT(16'h7444)) 
    q__7_i_1
       (.I0(tis100_n_2),
        .I1(s_axi_lite_bvalid),
        .I2(q_reg__5),
        .I3(tis100_n_4),
        .O(q__7_i_1_n_0));
  LUT4 #(
    .INIT(16'h88F8)) 
    q__8_i_1
       (.I0(tis100_n_2),
        .I1(s_axi_lite_bvalid),
        .I2(q_reg__8),
        .I3(tis100_n_3),
        .O(q__8_i_1_n_0));
  block_top_tis100_top_wrap_0_0_tis100_top tis100
       (.Q({tis100_n_16,tis100_n_17}),
        .clock(clock),
        .d015_out(d015_out),
        .dip(dip),
        .io_clk100(io_clk100),
        .led(led),
        .pb(pb),
        .\q_reg[0]__1_0 (\q_reg[0]__1 ),
        .\q_reg[1]__1_0 (\q_reg[1]__1 ),
        .\q_reg[1]__1_1 (tis100_n_52),
        .\q_reg[1]__5__0_0 (\q[1]__5_i_1_n_0 ),
        .\q_reg[1]__6_0 (\q[1]__6_i_1_n_0 ),
        .\q_reg[2]_0 (tis100_n_59),
        .\q_reg[2]__0_0 (\q_reg[2]__0 ),
        .\q_reg[31]__0_0 (q__2_i_2_n_0),
        .\q_reg[3]__0_0 (\q_reg[3]__0 ),
        .\q_reg[8]_0 (tis100_n_58),
        .\q_reg[8]__1_0 ({tis100_n_18,tis100_n_19}),
        .q_reg_0(tis100_n_0),
        .q_reg__0_0(tis100_n_1),
        .q_reg__1_0(q__1_i_1_n_0),
        .q_reg__2_0(q_reg__2),
        .q_reg__2_1(q__2_i_1_n_0),
        .q_reg__3_0(tis100_n_2),
        .q_reg__4_0(tis100_n_3),
        .q_reg__5_0(q_reg__5),
        .q_reg__5_1(q__5_i_1_n_0),
        .q_reg__6_0(tis100_n_4),
        .q_reg__7_0(q__7_i_1_n_0),
        .q_reg__8_0(q_reg__8),
        .q_reg__8_1(q__8_i_1_n_0),
        .s_axi_lite_araddr(s_axi_lite_araddr),
        .s_axi_lite_arvalid(s_axi_lite_arvalid),
        .s_axi_lite_awaddr(s_axi_lite_awaddr),
        .s_axi_lite_awvalid(s_axi_lite_awvalid),
        .s_axi_lite_bready(s_axi_lite_bready),
        .s_axi_lite_bresp(s_axi_lite_bresp),
        .s_axi_lite_bvalid(s_axi_lite_bvalid),
        .s_axi_lite_rdata(s_axi_lite_rdata),
        .s_axi_lite_rready(s_axi_lite_rready),
        .s_axi_lite_rresp(s_axi_lite_rresp),
        .s_axi_lite_rvalid(s_axi_lite_rvalid),
        .s_axi_lite_wdata(s_axi_lite_wdata),
        .s_axi_lite_wvalid(s_axi_lite_wvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif

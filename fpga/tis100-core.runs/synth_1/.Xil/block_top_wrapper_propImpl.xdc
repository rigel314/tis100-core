set_property SRC_FILE_INFO {cfile:/mnt/c/Users/test/source/tis100-core/fpga/tis100-core.srcs/sources_1/bd/block_top/ip/block_top_processing_system7_0_0/block_top_processing_system7_0_0/block_top_processing_system7_0_0_in_context.xdc rfile:../../../tis100-core.srcs/sources_1/bd/block_top/ip/block_top_processing_system7_0_0/block_top_processing_system7_0_0/block_top_processing_system7_0_0_in_context.xdc id:1 order:EARLY scoped_inst:block_top_i/processing_system7_0} [current_design]
set_property SRC_FILE_INFO {cfile:/mnt/c/Users/test/source/tis100-core/fpga/tis100-core.srcs/constrs_1/new/microzed_io.xdc rfile:../../../tis100-core.srcs/constrs_1/new/microzed_io.xdc id:2} [current_design]
current_instance block_top_i/processing_system7_0
set_property src_info {type:SCOPED_XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 10.000 [get_ports {}]
current_instance
set_property src_info {type:XDC file:2 line:4 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U14 [get_ports {LED[0]}];  # "U14.JX1_LVDS_10_P.JX1.41.LED0"
set_property src_info {type:XDC file:2 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U15 [get_ports {LED[1]}];  # "U15.JX1_LVDS_10_N.JX1.43.LED1"
set_property src_info {type:XDC file:2 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U18 [get_ports {LED[2]}];  # "U18.JX1_LVDS_11_P.JX1.42.LED2"
set_property src_info {type:XDC file:2 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U19 [get_ports {LED[3]}];  # "U19.JX1_LVDS_11_N.JX1.44.LED3"
set_property src_info {type:XDC file:2 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R19 [get_ports {LED[4]}];  # "R19.JX1_SE_0.JX1.9.LED4"
set_property src_info {type:XDC file:2 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V13 [get_ports {LED[5]}];  # "V13.JX1_LVDS_2_N.JX1.19.LED5"
set_property src_info {type:XDC file:2 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN P14 [get_ports {LED[6]}];  # "P14.JX1_LVDS_5_P.JX1.24.LED6"
set_property src_info {type:XDC file:2 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R14 [get_ports {LED[7]}];  # "R14.JX1_LVDS_5_N.JX1.26.LED7"
set_property src_info {type:XDC file:2 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN K17 [get_ports {io_clk100}];     # "K17.JX2_LVDS_11_P.JX2.48.BB_CLK"
set_property src_info {type:XDC file:2 line:18 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN K18 [get_ports {io_clk100_en}];  # "K18.JX2_LVDS_11_N.JX2.50.BB_CLK_EN"
set_property src_info {type:XDC file:2 line:25 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M14 [get_ports {DIP_SW[0]}];  # "M14.JX2_LVDS_22_P.JX2.87.DIP_SW0"
set_property src_info {type:XDC file:2 line:26 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M15 [get_ports {DIP_SW[1]}];  # "M15.JX2_LVDS_22_N.JX2.89.DIP_SW1"
set_property src_info {type:XDC file:2 line:27 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN K16 [get_ports {DIP_SW[2]}];  # "K16.JX2_LVDS_23_P.JX2.88.DIP_SW2"
set_property src_info {type:XDC file:2 line:28 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J16 [get_ports {DIP_SW[3]}];  # "J16.JX2_LVDS_23_N.JX2.90.DIP_SW3"
set_property src_info {type:XDC file:2 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G19 [get_ports {PB[0]}];  # "G19.JX2_LVDS_16_P.JX2.67.PB0"
set_property src_info {type:XDC file:2 line:35 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G20 [get_ports {PB[1]}];  # "G20.JX2_LVDS_16_N.JX2.69.PB1"
set_property src_info {type:XDC file:2 line:36 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J20 [get_ports {PB[2]}];  # "J20.JX2_LVDS_17_P.JX2.68.PB2"
set_property src_info {type:XDC file:2 line:37 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN H20 [get_ports {PB[3]}];  # "H20.JX2_LVDS_17_N.JX2.70.PB3"
set_property src_info {type:XDC file:2 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 34]];
set_property src_info {type:XDC file:2 line:46 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];
